/*
 * Copyright 2014, Broadcom Corporation
 * All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Broadcom Corporation;
 * the contents of this file may not be disclosed to third parties, copied
 * or duplicated in any form, in whole or in part, without the prior
 * written permission of Broadcom Corporation.
 */

/** @file
 *
 */
#include "stdint.h"
#include "string.h"
#include "platform_peripheral.h"
#include "platform_isr.h"
#include "platform_isr_interface.h"
#include "wwd_rtos.h"
#include "wwd_assert.h"
#include "cpal_i2c.h"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

#define I2C_FLAG_CHECK_TIMEOUT      ( 1000 )
#define I2C_FLAG_CHECK_LONG_TIMEOUT ( 1000 )
#define I2C_MESSAGE_DMA_MASK_POSN   ( 0 )
#define I2C_MESSAGE_NO_DMA          ( 0 << I2C_MESSAGE_DMA_MASK_POSN ) /* No DMA is set to 0 because DMA should be enabled by */
#define I2C_MESSAGE_USE_DMA         ( 1 << I2C_MESSAGE_DMA_MASK_POSN ) /* default, and turned off as an exception */
#define DMA_TIMEOUT_LOOPS           ( 10000000 )

#define I2C_TRANSFER_TIMEOUT 1000

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variables Definitions
 ******************************************************/

/******************************************************
 *               Function Definitions
 ******************************************************/

platform_result_t platform_i2c_init( const platform_i2c_t* i2c, const platform_i2c_config_t* config )
{
    CPAL_InitTypeDef* pDevInitStruct = i2c->cpal_i2c_struct;
    if(CPAL_I2C_StructInit(pDevInitStruct) != CPAL_PASS)
    {
        WPRINT_PLATFORM_DEBUG(("WICED_ERROR: file: %s, line: %d\r\n", __FILE__, __LINE__));
        return WICED_ERROR;
    }
    pDevInitStruct->pCPAL_TransferRx = i2c->cpal_transfer_rx_struct;
    pDevInitStruct->pCPAL_TransferTx = i2c->cpal_transfer_tx_struct;
    pDevInitStruct->pCPAL_I2C_Struct->I2C_ClockSpeed = 100000; //10 kHz
    pDevInitStruct->CPAL_ProgModel = CPAL_PROGMODEL_INTERRUPT;
    pDevInitStruct->wCPAL_Options |= CPAL_OPT_I2C_ERRIT_DISABLE;
    if(CPAL_I2C_Init(pDevInitStruct) != CPAL_PASS)
    {
        WPRINT_PLATFORM_DEBUG(("WICED_ERROR: file: %s, line: %d\r\n", __FILE__, __LINE__));
        return WICED_ERROR;
    }
    if ( host_rtos_init_semaphore( i2c->cpal_transfer_semaphore ) != WWD_SUCCESS )
    {
        WPRINT_PLATFORM_DEBUG(("WICED_ERROR: file: %s, line: %d\r\n", __FILE__, __LINE__));
        return WICED_ERROR;
    }
    return WICED_SUCCESS;
}

platform_result_t platform_i2c_deinit( const platform_i2c_t* i2c, const platform_i2c_config_t* config )
{
    wiced_assert( "bad argument", ( i2c != NULL ) && ( config != NULL ) );

    platform_mcu_powersave_disable( );

    /* Disable I2C peripheral clocks */
    RCC_APB1PeriphClockCmd( i2c->peripheral_clock_reg, DISABLE );

    /* Disable DMA */
    if ( config->flags & I2C_DEVICE_USE_DMA )
    {
        DMA_DeInit( i2c->rx_dma_stream );
        DMA_DeInit( i2c->tx_dma_stream );
        RCC_AHB1PeriphClockCmd( i2c->tx_dma_peripheral_clock, DISABLE );
    }

    platform_mcu_powersave_enable( );

    return PLATFORM_SUCCESS;
}

static platform_result_t i2c_wait_for_event( I2C_TypeDef* i2c, uint32_t event_id, uint32_t number_of_waits )
{
    while ( I2C_CheckEvent( i2c, event_id ) != SUCCESS )
    {
        number_of_waits--;
        if ( number_of_waits == 0 )
        {
            return PLATFORM_TIMEOUT;
        }
    }

    return PLATFORM_SUCCESS;
}

wiced_bool_t platform_i2c_probe_device( const platform_i2c_t* i2c, const platform_i2c_config_t* config, int retries )
{
    platform_result_t result;
    int i;

    wiced_assert( "bad argument", ( i2c != NULL ) && ( config != NULL ) );

    platform_mcu_powersave_disable();

    for ( i = 0; i < retries; i++ )
    {
        /* generate a start condition and address a i2c in write mode */
        I2C_GenerateSTART( i2c->port, ENABLE );

        /* wait till start condition is generated and the bus becomes free */
        result = i2c_wait_for_event( i2c->port, I2C_EVENT_MASTER_MODE_SELECT, I2C_FLAG_CHECK_TIMEOUT );
        if ( result != PLATFORM_SUCCESS )
        {
            // FIXME platform_mcu_powersave_enable();
            return WICED_FALSE;
        }

        if ( config->address_width == I2C_ADDRESS_WIDTH_7BIT )
        {
            /* send the address and R/W bit set to write of the requested i2c, wait for an acknowledge */
            I2C_Send7bitAddress( i2c->port, (uint8_t) ( config->address << 1 ), I2C_Direction_Transmitter );

            /* wait till address gets sent and the direction bit is sent and */
            result = i2c_wait_for_event( i2c->port, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED, I2C_FLAG_CHECK_LONG_TIMEOUT );
            if ( result != PLATFORM_SUCCESS )
            {
                /* keep on pinging */
                continue;
            }
            else
            {
                break;
            }
        }
        else
        {
            // TODO
            /* send 10 bits of the address and wait for an acknowledge */
        }
    }

    /* generate a stop condition */
    I2C_GenerateSTOP( i2c->port, ENABLE );

    platform_mcu_powersave_enable();

    /* Check if the i2c didn't respond */
    if ( i == retries )
    {
        return WICED_FALSE;
    }
    else
    {
        return WICED_TRUE;
    }
}

platform_result_t platform_i2c_init_tx_message( platform_i2c_message_t* message, void* tx_buffer, uint16_t tx_buffer_length, uint16_t retries, wiced_bool_t disable_dma )
{
    wiced_assert( "bad argument", ( message != NULL ) && ( tx_buffer != NULL ) && ( tx_buffer_length != 0 ) );

    memset( message, 0x00, sizeof( *message ) );
    message->tx_buffer = tx_buffer;
    message->combined  = WICED_FALSE;
    message->retries   = retries;
    message->tx_length = tx_buffer_length;

    if ( disable_dma )
    {
        message->flags = I2C_MESSAGE_NO_DMA;
    }
    else
    {
        message->flags = I2C_MESSAGE_USE_DMA;
    }

    return PLATFORM_SUCCESS;
}

platform_result_t platform_i2c_init_rx_message( platform_i2c_message_t* message, void* rx_buffer, uint16_t rx_buffer_length, uint16_t retries, wiced_bool_t disable_dma )
{
    wiced_assert( "bad argument", ( message != NULL ) && ( rx_buffer != NULL ) && ( rx_buffer_length != 0 ) );

    memset( message, 0x00, sizeof( *message ) );

    message->rx_buffer = rx_buffer;
    message->combined  = WICED_FALSE;
    message->retries   = retries;
    message->rx_length = rx_buffer_length;

    if ( disable_dma )
    {
        message->flags = I2C_MESSAGE_NO_DMA;
    }
    else
    {
        message->flags = I2C_MESSAGE_USE_DMA;
    }

    return PLATFORM_SUCCESS;
}

platform_result_t platform_i2c_init_combined_message( platform_i2c_message_t* message, const void* tx_buffer, void* rx_buffer, uint16_t tx_buffer_length, uint16_t rx_buffer_length, uint16_t retries, wiced_bool_t disable_dma )
{
    wiced_assert( "bad argument", ( message != NULL ) && ( tx_buffer != NULL ) && ( tx_buffer_length != 0 ) && ( rx_buffer != NULL ) && ( rx_buffer_length != 0 ) );

    memset( message, 0x00, sizeof( *message ) );

    message->rx_buffer = rx_buffer;
    message->tx_buffer = tx_buffer;
    message->combined  = WICED_TRUE;
    message->retries   = retries;
    message->tx_length = tx_buffer_length;
    message->rx_length = rx_buffer_length;

    if ( disable_dma )
    {
        message->flags = I2C_MESSAGE_NO_DMA;
    }
    else
    {
        message->flags = I2C_MESSAGE_USE_DMA;
    }

    return PLATFORM_SUCCESS;
}

platform_result_t platform_i2c_transfer( const platform_i2c_t* i2c, const platform_i2c_config_t* config, platform_i2c_message_t* messages, uint16_t number_of_messages )
{
    uint32_t result;
    UNUSED_PARAMETER( number_of_messages );
    if(messages->rx_buffer != NULL)
    {
        i2c->cpal_transfer_rx_struct->pbBuffer = (uint8_t *)messages->rx_buffer;
        i2c->cpal_transfer_rx_struct->wAddr1 = messages->slave_address;
        i2c->cpal_transfer_rx_struct->wAddr2 = messages->register_address;
        i2c->cpal_transfer_rx_struct->wNumData = messages->rx_length;
        result = CPAL_I2C_Read(i2c->cpal_i2c_struct);
        if(result != CPAL_PASS)
        {
            WPRINT_PLATFORM_DEBUG(("CPAL_I2C_Read() failed\r\n"));
            return WICED_ERROR;
        }
        return host_rtos_get_semaphore( i2c->cpal_transfer_semaphore, (uint32_t)I2C_TRANSFER_TIMEOUT, WICED_FALSE );
    }
    else if(messages->tx_buffer != NULL)
    {
        i2c->cpal_transfer_tx_struct->pbBuffer = (uint8_t *)messages->tx_buffer;
        i2c->cpal_transfer_tx_struct->wAddr1 = messages->slave_address;
        i2c->cpal_transfer_tx_struct->wAddr2 = messages->register_address;
        i2c->cpal_transfer_tx_struct->wNumData = messages->tx_length;
        result = CPAL_I2C_Write(i2c->cpal_i2c_struct);
        if(result != CPAL_PASS)
        {
            WPRINT_PLATFORM_DEBUG(("CPAL_I2C_Write() failed\r\n"));
            return WICED_ERROR;
        }
        return host_rtos_get_semaphore( i2c->cpal_transfer_semaphore, (uint32_t)I2C_TRANSFER_TIMEOUT, WICED_FALSE );
    }
    return WICED_SUCCESS;
}
