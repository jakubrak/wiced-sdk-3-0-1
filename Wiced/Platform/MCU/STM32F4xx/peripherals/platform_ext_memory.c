/*
 * Copyright 2014, Broadcom Corporation
 * All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Broadcom Corporation;
 * the contents of this file may not be disclosed to third parties, copied
 * or duplicated in any form, in whole or in part, without the prior
 * written permission of Broadcom Corporation.
 */
#include <stdint.h>
#include "wwd_constants.h"
#include "stm32f4xx_fsmc.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "platform_ext_memory.h"
#include "platform_external_memory.h"
#include <string.h>
#include "stdlib.h"

#ifdef __cplusplus
extern "C" {
#endif

//#define BCM9WCD1AUDIO

/******************************************************
 *                      Macros
 ******************************************************/
#define ARRAY_SIZE(a)         ( sizeof(a) / sizeof(a[0]) )

/******************************************************
 *                    Constants
 ******************************************************/
#define CHIP_SELECT_AREA_SIZE ( 0x4000000 ) /* 64 Mbytes for every chip select */

/* Chip select control registers */
#define FSMC_BCR1           ((volatile uint32_t*)(0xA0000000))
#define FSMC_BCR2           ((volatile uint32_t*)(0xA0000008))
#define FSMC_BCR3           ((volatile uint32_t*)(0xA0000010))
#define FSMC_BCR4           ((volatile uint32_t*)(0xA0000018))

/* Chip select timing */
#define FSMC_BTCR1          ((volatile uint32_t*)(0xA0000004))
#define FSMC_BTCR2          ((volatile uint32_t*)(0xA000000C))
#define FSMC_BTCR3          ((volatile uint32_t*)(0xA0000014))
#define FSMC_BTCR4          ((volatile uint32_t*)(0xA000001C))

/* Write timings for extended mode */
#define FSMC_BWTR1          ((volatile uint32_t*)(0xA0000104))
#define FSMC_BWTR2          ((volatile uint32_t*)(0xA000010C))
#define FSMC_BWTR3          ((volatile uint32_t*)(0xA0000114))
#define FSMC_BWTR4          ((volatile uint32_t*)(0xA000011C))

#define SRAM_BANK_ID        (0)
#define NAND2_BANKID        (1)
#define NAND1_BANKID        (2)
#define CLK_CLOCK           (1)
#define HCLK_CLOCK          (2)
#define MIN_DATLAT_CYCLES   (2)
#define MAX_DATLAT_CYCLES   (17)
#define MIN_BUSTURN_CYCLES  (0)
#define MAX_BUSTURN_CYCLES  (15)
#define MIN_DATAST_CYCLES   (1)
#define MAX_DATAST_CYCLES   (255)
#define MIN_ADDHLD_CYCLES   (1)
#define MAX_ADDHLD_CYCLES   (255)
#define MIN_ADDSET_CYCLES   (0)
#define MAX_ADDSET_CYCLES   (1615)


/* register fields - BCR */
#define CBURSTRW_START      (19)
#define ASYNCWAIT_START     (15)
#define EXTMOD_START        (14)
#define WAITEN_START        (13)
#define WAITEN_START        (13)
#define WREN_START          (12)
#define WAITCFG_START       (11)
#define WRAPMOD_START       (10)
#define WAITPOL_START       (9)
#define BURSTEN_START       (8)
#define FACCEN_START      (6)
#define MWID_START        (4)
#define MTYP_START        (2)
#define MUXEN_START       (1)
#define MBKEN_START       (0)

/* register fields - BTR, general timing settings */
#define ACCMOD_START      (28)
#define DATLAT_START      (24)
#define CLKDIV_START      (20)
#define BUSTURN_START     (16)
#define DATAST_START      (8)
#define ADDHLD_START      (4)
#define ADDSET_START      (0)

/* register fields BWTR, timing settings for write transactions */
#define ACCMOD_WR_START   (28)
#define DATLAT_WR_START   (24)
#define CLKDIV_WR_START   (20)
#define BUSTURN_WR_START  (16)
#define DATAST_WR_START   (8)
#define ADDHLD_WR_START   (4)
#define ADDSET_WR_START   (0)

/******************************************************
 *                   Enumerations
 ******************************************************/
typedef enum
{
    GPIOA_index,
    GPIOB_index,
    GPIOC_index,
    GPIOD_index,
    GPIOE_index,
    GPIOF_index,
    GPIOG_index,
    GPIOH_index,
    GPIOI_index,
} pin_registerer_indexes;

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/
typedef struct
{
    /* gpio related data */
    GPIO_TypeDef* gpio_port;
    pin_registerer_indexes index;
    uint16_t      gpio_pin;
} pin_info_t;

typedef struct
{
    GPIO_TypeDef * port;
    uint32_t       pins;
} port_utilization_t;

typedef struct
{
    volatile uint32_t* bcr;
    volatile uint32_t* btcr;
    volatile uint32_t* bwtr;
} stm32f4_sram_nor_control_t;

/******************************************************
 *               Function Declarations
 ******************************************************/

int fsmc_sram_configure( const wiced_sram_device_t* sram, const stm32f4xx_platform_nor_sram_t* settings );
int f4_sram_timings_init ( const wiced_sram_device_t* sram, const stm32f4xx_platform_nor_sram_t* settings );

/******************************************************
 *               Variable Definitions
 ******************************************************/

/* Only four chip select pins are available NE1-NE4 */
pin_info_t chip_select_pins[4]=
{
        {
                /* Pin NE1 */
                .gpio_port = GPIOD,
                .index = GPIOD_index,
                .gpio_pin = 7
        },
        {
                /* Pin NE2 */
                .gpio_port = GPIOG,
                .index = GPIOG_index,
                .gpio_pin = 9

        },
        {
                /* Pin NE3 */
                .gpio_port = GPIOG,
                .index = GPIOG_index,
                .gpio_pin = 10
        },
        {
                /* Pin NE4 */
                .gpio_port = GPIOG,
                .index = GPIOG_index,
                .gpio_pin = 12
        },
};


const stm32f4_sram_nor_control_t f2_sram_nor_control_regs[] =
{

        /* NE1 chip select control registers */
        {
                .bcr    = FSMC_BCR1,
                .btcr   = FSMC_BTCR1,
                .bwtr   = FSMC_BWTR1,
        },

        /* NE2 chip select control registers */
        {
                .bcr    = FSMC_BCR2,
                .btcr   = FSMC_BTCR2,
                .bwtr   = FSMC_BWTR2,
        },
        /* NE3 chip select */
        {
                .bcr    = FSMC_BCR3,
                .btcr   = FSMC_BTCR3,
                .bwtr   = FSMC_BWTR3,
        },
        /* NE4 chip select */
        {
                .bcr    = FSMC_BCR4,
                .btcr   = FSMC_BTCR4,
                .bwtr   = FSMC_BWTR4,
        }
};


static int ns_to_cycles( unsigned int num_ns, int* cycles, int max, int min, int clock_type )
{
    UNUSED_PARAMETER(max);
    UNUSED_PARAMETER(min);
    UNUSED_PARAMETER(clock_type);

    /* Should be fixed */
    *cycles = (int)num_ns;
    return 0;
}

int f4_sram_timings_init ( const wiced_sram_device_t* sram, const stm32f4xx_platform_nor_sram_t* settings )
{
    int retval = 0;
    int temp;
    const stm32f4_sram_nor_control_t* regs = &f2_sram_nor_control_regs[sram->chip_select];

    /* WAICFG bit is untouched currently */
    ( settings->wr_burst            == 1 ) ? ( *regs->bcr |= ( uint32_t )( 1 << CBURSTRW_START ) )  : ( *regs->bcr &= ( uint32_t )(~( 1 << CBURSTRW_START )) );
    ( settings->async_wait_enabled  == 1 ) ? ( *regs->bcr |= ( uint32_t )( 1 << ASYNCWAIT_START ) ) : ( *regs->bcr &= ( uint32_t )(~( 1 << ASYNCWAIT_START )) );
    ( settings->extended_mode       == 1 ) ? ( *regs->bcr |= ( uint32_t )( 1 << EXTMOD_START ) )    : ( *regs->bcr &= ( uint32_t )(~( 1 << EXTMOD_START )) );
    ( settings->sync_wait_enabled   == 1 ) ? ( *regs->bcr |= ( uint32_t )( 1 << WAITEN_START ) )    : ( *regs->bcr &= ( uint32_t )(~( 1 << WAITEN_START )) );
    ( settings->wait_polarity       == 1 ) ? ( *regs->bcr |= ( uint32_t )( 1 << WAITPOL_START ) )   : ( *regs->bcr &= ( uint32_t )(~( 1 << WAITPOL_START )) );
    ( settings->rd_burst            == 1 ) ? ( *regs->bcr |= ( uint32_t )( 1 << BURSTEN_START ) )   : ( *regs->bcr &= ( uint32_t )(~( 1 << BURSTEN_START )) );

    /* This is nor a nor flash */
    *regs->bcr &= ( uint32_t )( ~( 1 << FACCEN_START ) );
    /* Clear MTYP, must be 0 */
    *regs->bcr &= ( uint32_t )( ~( 0x03 << MTYP_START ) );

    /* Select access mode */
    *regs->btcr &= ( 0x03 << ACCMOD_START ); /* !!!! must be bit-wise inversion */
    *regs->btcr |= ( uint32_t )( settings->access_mode << ACCMOD_START );
    /* Select data-latency */
    retval = ns_to_cycles( settings->data_latency, &temp, MAX_DATLAT_CYCLES, MIN_DATLAT_CYCLES, CLK_CLOCK );
    if( retval != 0 )
    {
        return retval;
    }
    /* Select clk divider for synchronous memories  */
    /* TODO */

    /* Select bus-turnaround duration */
    retval = ns_to_cycles( settings->bus_turnaround, &temp, MAX_BUSTURN_CYCLES, MIN_BUSTURN_CYCLES, HCLK_CLOCK );
    if( retval != 0 )
    {
        return retval;
    }
    *regs->btcr &= ( uint32_t )( ~( 0x0F << BUSTURN_START ) );
    *regs->btcr |= ( uint32_t )( ( temp << BUSTURN_START ) );

    /* Select data phase duration */
    retval = ns_to_cycles( settings->data_phase_duration, &temp, MAX_DATAST_CYCLES, MIN_DATAST_CYCLES, HCLK_CLOCK );
    if( retval != 0 )
    {
        return retval;
    }
    /* 8 bits are used for DATAST duration field */
    *regs->btcr &= ( uint32_t )( ~( 0xFF << DATAST_START ) );
    *regs->btcr |= ( uint32_t )( temp << DATAST_START );

    /* Select address hold duration */
    retval = ns_to_cycles( settings->address_hold_duration, &temp, MAX_ADDHLD_CYCLES, MIN_ADDHLD_CYCLES, HCLK_CLOCK );
    if( retval != 0 )
    {
        return retval;
    }
    *regs->btcr &= ( uint32_t )( ~( 0x0F << ADDHLD_START ) );
    *regs->btcr |= ( uint32_t )( temp << ADDHLD_START );

    /* Select address setup phase duration */
    retval = ns_to_cycles( settings->address_set_duration, &temp, MAX_ADDSET_CYCLES, MIN_ADDSET_CYCLES, HCLK_CLOCK );
    /* Check whether the addset duration will be in the available HCL range */
    if( retval != 0 )
    {
        return retval;
    }
    if( temp >= 15 )
    {
        /* Set to maximum available */
        temp = 15;
    }
    *regs->btcr &= ( uint32_t )( ~( 0x0F << ADDSET_START ) );
    *regs->btcr |= ( uint32_t )( temp << ADDSET_START );

    /* in extended mode, write memory transactions timings are different from read memory transactions timings */

    if( settings->extended_mode == 1 )
    {
        /* Select access mode */
        *regs->bwtr &= ( 0x03 << ACCMOD_START );
        *regs->bwtr |= ( uint32_t )( ( settings->wr_access_mode << ACCMOD_START ) );
        /* Select data-latency */
        retval = ns_to_cycles( settings->wr_data_latency, &temp, MAX_DATLAT_CYCLES, MIN_DATLAT_CYCLES, CLK_CLOCK );
        if( retval != 0 )
        {
            return retval;
        }
        /* Select clk divider for synchronous memories  */
        /* ???????? */

        /* Select bus-turnaround duration */
        retval = ns_to_cycles( settings->wr_bus_turnaround, &temp, MAX_BUSTURN_CYCLES, MIN_BUSTURN_CYCLES, HCLK_CLOCK );
        if( retval != 0 )
        {
            return retval;
        }
        *regs->bwtr &= ( uint32_t )( ~( 0x0F << BUSTURN_START ) );
        *regs->bwtr |= ( uint32_t )( temp << BUSTURN_START );

        /* Select data phase duration */
        retval = ns_to_cycles( settings->wr_data_phase_duration, &temp, MAX_DATAST_CYCLES, MIN_DATAST_CYCLES, HCLK_CLOCK );
        if( retval != 0 )
        {
            return retval;
        }
        /* 8 bits are used for DATAST duration field */
        *regs->bwtr &= ( uint32_t )( ~( 0xFF << DATAST_START ) );
        *regs->bwtr |= ( uint32_t )( temp << DATAST_START );

        /* Select address hold duration */
        retval = ns_to_cycles( settings->wr_address_hold_duration, &temp, MAX_ADDHLD_CYCLES, MIN_ADDHLD_CYCLES, HCLK_CLOCK );
        if( retval != 0 )
        {
            return retval;
        }
        *regs->bwtr &= ( uint32_t )( ~( 0x0F << ADDHLD_START ) );
        *regs->bwtr |= ( uint32_t )( temp << ADDHLD_START );

        /* Select address setup phase duration */
        retval = ns_to_cycles( settings->wr_address_set_duration, &temp, MAX_ADDSET_CYCLES, MIN_ADDSET_CYCLES, HCLK_CLOCK );
        /* get the ratio */
        retval = ( temp * 1615 ) / 15;
        if( retval != 0 )
        {
            return retval;
        }
        *regs->bwtr &= ( uint32_t )( ~( 0x0F << ADDSET_START ) );
        *regs->bwtr |= ( uint32_t )( temp << ADDSET_START );
    }

    return 0;
}

#if 0
int fsmc_sram_configure( const wiced_sram_device_t* sram, const stm32f4xx_platform_nor_sram_t* settings )
{
    int retval;
    const stm32f4_sram_nor_control_t* regs = &f2_sram_nor_control_regs[sram->chip_select];

    /* Enable FSMC clock */
    RCC_AHB3PeriphClockCmd(RCC_AHB3Periph_FSMC, ENABLE);

    /* Clear and then set bus data width */
    *( regs->bcr ) &= (uint32_t)( ~( 0x03 << MWID_START ) );
    ( sram->bus_data_width == 16 ) ? ( *( regs->bcr ) |= ( uint32_t )( 1 << MWID_START ) ) : ( *( regs->bcr ) &= ( uint32_t )( ~( 1 << MWID_START ) ) ) ;

    /* enable writes on this memory device. IT is RAM */
    *(regs->bcr) |= ( uint32_t )( 1 << WREN_START );
    /* check whether address lines are multiplexed with data lines and enable must functionality */
    ( sram->mux_addr_data == 1 ) ? ( *(regs->bcr) |= ( 1 << MUXEN_START ) ) : ( *(regs->bcr) &= ( uint32_t )( ~( 1 << MUXEN_START ) ) );

    retval = f4_sram_timings_init( sram, settings );
    if ( retval != 0 )
    {
         goto error_handling;
    }
    f4_sram_pins_init( sram, settings );

    /* enable this memory bank, nor a chip select( for every memory bank we can have 4 chip selects ) */
    /* But still remember that they are all shared between all 4 banks. Once used for one of the banks */
    /* chip select can not be used again */
    *(regs->bcr) |= ( 1 << MBKEN_START );

    return 0;

error_handling:

    RCC_AHB3PeriphClockCmd(RCC_AHB3Periph_FSMC, DISABLE);
    return retval;
}


static int f4_nor_configure( wiced_nor_device_t* nor, stm32f4xx_platform_nor_sram_t* settings )
{
    UNUSED_PARAMETER(nor);
    UNUSED_PARAMETER(settings);

    return 0;

}

static int f4_nand_configure( wiced_nand_device_t* nand, stm32f4xx_platform_nor_sram_t* settings )
{
    UNUSED_PARAMETER(nand);
    UNUSED_PARAMETER(settings);

    return WICED_UNSUPPORTED;
}
#endif

wiced_result_t ext_memory_configure( )
{
	GPIO_InitTypeDef GPIO_InitStructure;
	FSMC_NORSRAMInitTypeDef  FSMC_NORSRAMInitStructure;
	FSMC_NORSRAMTimingInitTypeDef FSMC_NORSRAMTimingInitStructure;

	/****** Configures the FSMC GPIOs ******/
	/****** SRAM Data lines,  NOE and NWE configuration ******/

	/*** Connect FSMC pins to AF12 ***/
	/* D0..3,13..15, NOE(PD4), NWE(PD5), NE1(PD7), A16(PD11) */
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource0, GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource1, GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource4, GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource5, GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource7, GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource8, GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource9, GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource10, GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource11, GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource14, GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource15, GPIO_AF_FSMC);
	/* D4..12 */
	GPIO_PinAFConfig(GPIOE, GPIO_PinSource7 , GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOE, GPIO_PinSource8 , GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOE, GPIO_PinSource9 , GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOE, GPIO_PinSource10 , GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOE, GPIO_PinSource11 , GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOE, GPIO_PinSource12 , GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOE, GPIO_PinSource13 , GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOE, GPIO_PinSource14 , GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOE, GPIO_PinSource15 , GPIO_AF_FSMC);

	/*** FSMC GPIOs configuration ***/
	/* D0..3,13..15, NOE(PD4), NWE(PD5), NE1(PD7), A16(PD11) */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_8 | GPIO_Pin_9 |
								GPIO_Pin_10 | GPIO_Pin_14 | GPIO_Pin_15 |
								GPIO_Pin_4 |GPIO_Pin_5 | GPIO_Pin_7 | GPIO_Pin_11;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOD, &GPIO_InitStructure);
	/* D4..12 */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 |
								GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 |
								GPIO_Pin_15;
	GPIO_Init(GPIOE, &GPIO_InitStructure);

	/* LCD_RESET line configuration */
	GPIOD->MODER |= GPIO_MODER_MODER13_0;
	GPIOD->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR13_1;

	RCC_AHB3PeriphClockCmd(RCC_AHB3Periph_FSMC, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOB |
					 RCC_AHB1Periph_GPIOC | RCC_AHB1Periph_GPIOD |
					 RCC_AHB1Periph_GPIOE, ENABLE);

	FSMC_NORSRAMTimingInitStructure.FSMC_AddressSetupTime = 10;
	FSMC_NORSRAMTimingInitStructure.FSMC_AddressHoldTime = 0;
	FSMC_NORSRAMTimingInitStructure.FSMC_DataSetupTime = 10;
	FSMC_NORSRAMTimingInitStructure.FSMC_BusTurnAroundDuration = 0x00;
	FSMC_NORSRAMTimingInitStructure.FSMC_CLKDivision = 0x00;
	FSMC_NORSRAMTimingInitStructure.FSMC_DataLatency = 0x00;
	FSMC_NORSRAMTimingInitStructure.FSMC_AccessMode = FSMC_AccessMode_A;

	FSMC_NORSRAMInitStructure.FSMC_Bank = FSMC_Bank1_NORSRAM1;
	FSMC_NORSRAMInitStructure.FSMC_DataAddressMux = FSMC_DataAddressMux_Disable;
	FSMC_NORSRAMInitStructure.FSMC_MemoryType = FSMC_MemoryType_SRAM;
	FSMC_NORSRAMInitStructure.FSMC_MemoryDataWidth = FSMC_MemoryDataWidth_16b;
	FSMC_NORSRAMInitStructure.FSMC_BurstAccessMode = FSMC_BurstAccessMode_Disable;
	FSMC_NORSRAMInitStructure.FSMC_WaitSignalPolarity = FSMC_WaitSignalPolarity_Low;
	FSMC_NORSRAMInitStructure.FSMC_WrapMode = FSMC_WrapMode_Disable;
	FSMC_NORSRAMInitStructure.FSMC_WaitSignalActive = FSMC_WaitSignalActive_BeforeWaitState;
	FSMC_NORSRAMInitStructure.FSMC_WriteOperation = FSMC_WriteOperation_Enable;
	FSMC_NORSRAMInitStructure.FSMC_WaitSignal = FSMC_WaitSignal_Disable;
	FSMC_NORSRAMInitStructure.FSMC_AsynchronousWait = FSMC_AsynchronousWait_Disable;
	FSMC_NORSRAMInitStructure.FSMC_ExtendedMode = FSMC_ExtendedMode_Disable;
	FSMC_NORSRAMInitStructure.FSMC_WriteBurst = FSMC_WriteBurst_Disable;
	FSMC_NORSRAMInitStructure.FSMC_ReadWriteTimingStruct = &FSMC_NORSRAMTimingInitStructure;
	FSMC_NORSRAMInit(&FSMC_NORSRAMInitStructure);

	FSMC_NORSRAMTimingInitStructure.FSMC_AddressSetupTime = 3;
	FSMC_NORSRAMTimingInitStructure.FSMC_AddressHoldTime = 0;
	FSMC_NORSRAMTimingInitStructure.FSMC_DataSetupTime = 3;
	FSMC_NORSRAMTimingInitStructure.FSMC_BusTurnAroundDuration = 0x00;
	FSMC_NORSRAMTimingInitStructure.FSMC_CLKDivision = 0x00;
	FSMC_NORSRAMTimingInitStructure.FSMC_DataLatency = 0x00;
	FSMC_NORSRAMTimingInitStructure.FSMC_AccessMode = FSMC_AccessMode_A;
	FSMC_NORSRAMInitStructure.FSMC_WriteTimingStruct = &FSMC_NORSRAMTimingInitStructure;

	FSMC_NORSRAMInit(&FSMC_NORSRAMInitStructure);

	/* Enable FSMC Bank1_SRAM Bank */
	FSMC_NORSRAMCmd(FSMC_Bank1_NORSRAM1, ENABLE);

	return WICED_SUCCESS;
}


#ifdef __cplusplus
} /* extern "C" */
#endif
