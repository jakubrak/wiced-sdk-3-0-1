/*
 * Copyright 2014, Broadcom Corporation
 * All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Broadcom Corporation;
 * the contents of this file may not be disclosed to third parties, copied
 * or duplicated in any form, in whole or in part, without the prior
 * written permission of Broadcom Corporation.
 */

/** @file
 *
 */
#include "stdint.h"
#include "string.h"
#include "platform_peripheral.h"
#include "platform_isr.h"
#include "platform_isr_interface.h"
#include "wwd_assert.h"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variables Definitions
 ******************************************************/

/******************************************************
 *               Function Definitions
 ******************************************************/

platform_result_t platform_pwm_init( const platform_pwm_t* pwm, uint32_t frequency, float duty_cycle )
{
    TIM_TimeBaseInitTypeDef tim_time_base_structure;
    TIM_OCInitTypeDef       tim_oc_init_structure;
    RCC_ClocksTypeDef       rcc_clock_frequencies;
    uint32_t                period              = 0;
    uint16_t prescaler = 0;
    float                   adjusted_duty_cycle = ( ( duty_cycle > 100.0f ) ? 100.0f : duty_cycle );

    wiced_assert( "bad argument", pwm != NULL );

    platform_mcu_powersave_disable();

    RCC_GetClocksFreq( &rcc_clock_frequencies );

    if ( pwm->tim == TIM1 || pwm->tim == TIM8 || pwm->tim == TIM9 || pwm->tim == TIM10 || pwm->tim == TIM11 )
    {
        RCC_APB2PeriphClockCmd( pwm->tim_peripheral_clock, ENABLE );
        period = (2 * rcc_clock_frequencies.PCLK2_Frequency) / frequency - 1;
        if(period > 65535)
        {
            prescaler = (uint16_t) (period / 65535);
            period = period / (uint32_t)(prescaler + 1);
        }
    }
    else
    {
        RCC_APB1PeriphClockCmd( pwm->tim_peripheral_clock, ENABLE );
        period = (2 * rcc_clock_frequencies.PCLK1_Frequency) / frequency - 1;
        if(period > 65535)
        {
            prescaler = (uint16_t) (period / 65535);
            period = (uint32_t) period / (uint32_t)(prescaler + 1);
        }
    }

    /* Set alternate function */
    platform_gpio_set_alternate_function( pwm->pin->port, pwm->pin->pin_number, GPIO_OType_PP, GPIO_PuPd_UP, pwm->gpio_af );

    /* Time base configuration */
    tim_time_base_structure.TIM_Period            = (uint32_t) period;
    tim_time_base_structure.TIM_Prescaler         = (uint16_t) prescaler;  /* Divide clock by 1+1 to enable a count of high cycle + low cycle = 1 PWM cycle */
    tim_time_base_structure.TIM_ClockDivision     = 0;
    tim_time_base_structure.TIM_CounterMode       = TIM_CounterMode_Up;
    tim_time_base_structure.TIM_RepetitionCounter = 0;
    TIM_TimeBaseInit( pwm->tim, &tim_time_base_structure );

    /* PWM1 Mode configuration */
    tim_oc_init_structure.TIM_OCMode       = TIM_OCMode_PWM1;
    tim_oc_init_structure.TIM_OutputState  = TIM_OutputState_Enable;
    tim_oc_init_structure.TIM_OutputNState = TIM_OutputNState_Enable;
    tim_oc_init_structure.TIM_Pulse        = (uint16_t) ( adjusted_duty_cycle * (float) period / 100.0f );
    tim_oc_init_structure.TIM_OCPolarity   = TIM_OCPolarity_High;
    tim_oc_init_structure.TIM_OCNPolarity  = TIM_OCNPolarity_High;
    tim_oc_init_structure.TIM_OCIdleState  = TIM_OCIdleState_Reset;
    tim_oc_init_structure.TIM_OCNIdleState = TIM_OCIdleState_Set;

    switch ( pwm->channel )
    {
        case 1:
        {
            TIM_OC1Init( pwm->tim, &tim_oc_init_structure );
            TIM_OC1PreloadConfig( pwm->tim, TIM_OCPreload_Enable );
            break;
        }
        case 2:
        {
            TIM_OC2Init( pwm->tim, &tim_oc_init_structure );
            TIM_OC2PreloadConfig( pwm->tim, TIM_OCPreload_Enable );
            break;
        }
        case 3:
        {
            TIM_OC3Init( pwm->tim, &tim_oc_init_structure );
            TIM_OC3PreloadConfig( pwm->tim, TIM_OCPreload_Enable );
            break;
        }
        case 4:
        {
            TIM_OC4Init( pwm->tim, &tim_oc_init_structure );
            TIM_OC4PreloadConfig( pwm->tim, TIM_OCPreload_Enable );
            break;
        }
        default:
        {
            break;
        }
    }

    platform_mcu_powersave_enable();

    return PLATFORM_SUCCESS;
}

platform_result_t platform_pwm_start( const platform_pwm_t* pwm )
{
    wiced_assert( "bad argument", pwm != NULL );

    platform_mcu_powersave_disable();

    TIM_Cmd( pwm->tim, ENABLE );
    TIM_CtrlPWMOutputs( pwm->tim, ENABLE );

    platform_mcu_powersave_enable();

    return PLATFORM_SUCCESS;
}

platform_result_t platform_pwm_stop( const platform_pwm_t* pwm )
{
    wiced_assert( "bad argument", pwm != NULL );

    platform_mcu_powersave_disable();

    TIM_CtrlPWMOutputs( pwm->tim, DISABLE );
    TIM_Cmd( pwm->tim, DISABLE );

    platform_mcu_powersave_enable();

    return PLATFORM_SUCCESS;
}

platform_result_t platform_pwm_update_duty_cycle( const platform_pwm_t* pwm, float duty_cycle )
{
    switch ( pwm->channel )
    {
        case 1:
            pwm->tim->CCR1 = (uint16_t) ( duty_cycle * (float) pwm->tim->ARR / 100.0f );
            break;
        case 2:
            pwm->tim->CCR2 = (uint16_t) ( duty_cycle * (float) pwm->tim->ARR / 100.0f );
            break;
        case 3:
            pwm->tim->CCR3 = (uint16_t) ( duty_cycle * (float) pwm->tim->ARR / 100.0f );
            break;
        case 4:
            pwm->tim->CCR4 = (uint16_t) ( duty_cycle * (float) pwm->tim->ARR / 100.0f );
            break;
        default:
            break;
    }
    return PLATFORM_SUCCESS;
}

