/*
 * Copyright 2014, Broadcom Corporation
 * All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Broadcom Corporation;
 * the contents of this file may not be disclosed to third parties, copied
 * or duplicated in any form, in whole or in part, without the prior
 * written permission of Broadcom Corporation.
 */

/** @file
 *
 */
#include "stdint.h"
#include "string.h"
#include "platform_peripheral.h"
#include "platform_isr.h"
#include "platform_isr_interface.h"
#include "wwd_assert.h"

extern void LCD_WriteRAM_Prepare(void);
extern void LCD_SetCursor(uint16_t Xpos, uint16_t Ypos);
extern void LCD_SetDisplayWindow(uint16_t Xpos, uint16_t Ypos, uint8_t Height, uint16_t Width);

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/
#define FRAME_MAX_SIZE 10*1024 //in 32-bit words

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variables Definitions
 ******************************************************/
uint32_t frame_buffer[FRAME_MAX_SIZE];
void (*frame_received)(void *arg) = NULL;
uint32_t cycles_count = 0;
host_semaphore_type_t snapshot_semaphore;
wiced_bool_t is_snapshot_mode = 0;
/******************************************************
 *               Function Definitions
 ******************************************************/

platform_result_t platform_dcmi_init(void (*frame_received_callback)(void *arg))
{
	GPIO_InitTypeDef GPIO_InitStructure;
	DCMI_InitTypeDef DCMI_InitStructure;
	DMA_InitTypeDef  DMA_InitStructure;

	frame_received = frame_received_callback;
	host_rtos_init_semaphore(&snapshot_semaphore);

	/*** Connect DCMI pins to AF13 ***/
	/* HSYNC(PA4), PIXCLK(PA6) */
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource4, GPIO_AF_DCMI);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource6, GPIO_AF_DCMI);
	/* D5(PB6), VSYNC(PB7) */
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_DCMI);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource7, GPIO_AF_DCMI);
	/* D0..3(PC6/7/8/9) */
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource6, GPIO_AF_DCMI);
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource7, GPIO_AF_DCMI);
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource8, GPIO_AF_DCMI);
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource9, GPIO_AF_DCMI);
	/* D4,6..7(PE4/5/6) */
	GPIO_PinAFConfig(GPIOE, GPIO_PinSource4, GPIO_AF_DCMI);
	GPIO_PinAFConfig(GPIOE, GPIO_PinSource5, GPIO_AF_DCMI);
	GPIO_PinAFConfig(GPIOE, GPIO_PinSource6, GPIO_AF_DCMI);

	/*** DCMI GPIOs configuration ***/
	/* HSYNC(PA4), PIXCLK(PA6) */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_6;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	/* D5(PB6), VSYNC(PB7) */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	/* D0..3(PC6/7/8/9) */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
	/* D4,6..7(PE4/5/6) */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6;
	GPIO_Init(GPIOE, &GPIO_InitStructure);

	/* Enable GPIOs clock */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOB |
			RCC_AHB1Periph_GPIOC | RCC_AHB1Periph_GPIOE, ENABLE);

	/* Enable DCMI clock */
	RCC_AHB2PeriphClockCmd(RCC_AHB2Periph_DCMI, ENABLE);

	/* Enable DMA2 clock */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);

	/*** Configures the DCMI to receive data from MT9D111 ***/
	DCMI_DeInit();

	/* DCMI configuration */
	DCMI_InitStructure.DCMI_CaptureMode = /*DCMI_CaptureMode_SnapShot;//*/DCMI_CaptureMode_Continuous;
	DCMI_InitStructure.DCMI_SynchroMode = DCMI_SynchroMode_Hardware;
	DCMI_InitStructure.DCMI_PCKPolarity = DCMI_PCKPolarity_Rising;        // Read data on rising edge of PIXCLK
	DCMI_InitStructure.DCMI_VSPolarity  = DCMI_VSPolarity_Low;            // Data valid on VSYNC high
	DCMI_InitStructure.DCMI_HSPolarity  = DCMI_HSPolarity_Low;            // Data valid on HSYNC high
	DCMI_InitStructure.DCMI_CaptureRate = DCMI_CaptureRate_All_Frame;
	DCMI_InitStructure.DCMI_ExtendedDataMode = DCMI_ExtendedDataMode_8b;  // 8-bit data interface
	DCMI_Init(&DCMI_InitStructure);

	DCMI_JPEGCmd(ENABLE);

	/* Enable interrupts from DCMI interface */
	/* OPCJONALNIE */
	DCMI_ITConfig(DCMI_IT_FRAME, ENABLE); // Przerwanie generowane po odebraniu jednej ramki obrazu
	//DCMI_ITConfig(DCMI_IT_VSYNC, ENABLE); // Przerwanie generowane przy przejsciu sygnalu VSYNC ze stanu aktywnego do nieaktywnego
	//DCMI_ITConfig(DCMI_IT_LINE, ENABLE);  // Przerwanie generowane po odebraniu jednego wiersza danych obrazu
	//DCMI_ITConfig(DCMI_IT_OVF, ENABLE);
	//DCMI_ITConfig(DCMI_IT_ERR, ENABLE);
	/* OPCJONALNIE */

	NVIC_EnableIRQ(DCMI_IRQn);

	/*** Configures the DMA2 to transfer data from DCMI ***/

	/* DMA2 Stream1 DeInit */
	DMA_DeInit(DMA2_Stream1);
	while (DMA_GetCmdStatus(DMA2_Stream1) != DISABLE);

	/* DMA2 Stream1 configuration */
	DMA_InitStructure.DMA_Channel = DMA_Channel_1;
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)(DCMI_BASE + 0x28);  // Rejestr 32-bitowy
	DMA_InitStructure.DMA_Memory0BaseAddr = /*(uint32_t)&frame_buffer;//*/0x60020000;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;                   // DCMI ---> Buffer
	DMA_InitStructure.DMA_BufferSize = /*FRAME_MAX_SIZE;//*/1;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Disable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Word;   // 32-bit
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;       // 32-bit
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_Low;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Enable;
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_Full;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_Init(DMA2_Stream1, &DMA_InitStructure);

	//DMA_ITConfig(DMA2_Stream1, DMA_IT_TC, ENABLE);
	//NVIC_EnableIRQ(DMA2_Stream1_IRQn);

	/* Enable DMA2 Stream 1 */
	DMA_Cmd(DMA2_Stream1, ENABLE);

    DCMI_Cmd(ENABLE);
    DCMI_CaptureCmd(ENABLE);

    return PLATFORM_SUCCESS;
}

void platform_dcmi_take_snapshot()
{
	LCD_SetDisplayWindow(239, 319, 240, 320);
	LCD_SetCursor(0, 319);
	LCD_WriteRAM_Prepare();
    DCMI_CaptureCmd(DISABLE);                 // Wylacz przyjmowanie danych obrazu
    DCMI_Cmd(DISABLE);                        // Wylacz DCMI
    DCMI->CR |= DCMI_CaptureMode_SnapShot;    // Wybierz tryb wykonania zdjecia
    is_snapshot_mode = 1;
    DCMI_Cmd(ENABLE);                         // Wlacz DCMI
    DCMI_CaptureCmd(ENABLE);                  // Wlacz przyjmowanie danych obrazu
    host_rtos_get_semaphore(&snapshot_semaphore, NEVER_TIMEOUT, WICED_TRUE);
    is_snapshot_mode = 0;
}

void platform_dcmi_preview()
{
	LCD_SetDisplayWindow(239, 319, 240, 320);
    LCD_SetCursor(0, 319);
    LCD_WriteRAM_Prepare();
    DCMI_CaptureCmd(DISABLE);
    DCMI_Cmd(DISABLE);
    DCMI->CR &= 0xFFFD;
    DCMI_Cmd(ENABLE);
    DCMI_CaptureCmd(ENABLE);
}

void DCMI_irq(void)
{
	// Przerwanie generowane po odebraniu pelnej ramki
	if(DCMI_GetITStatus(DCMI_IT_FRAME) == SET)
	{
		DCMI_ClearITPendingBit(DCMI_IT_FRAME);

	    //while(DMA_GetFlagStatus(DMA2_Stream1,DMA_FLAG_TCIF1) == RESET);
	    LCD_SetCursor(0, 319);
	    LCD_WriteRAM_Prepare();   // Prepare to write GRAM
//		uint16_t dma_remaining_data_count = DMA_GetCurrDataCounter(DMA2_Stream1);
//		uint32_t frame_size = 4 * ( (cycles_count - 1) * FRAME_MAX_SIZE +
//				FRAME_MAX_SIZE - dma_remaining_data_count);
//		frame_received(&frame_size);
		cycles_count = 0;
		if(is_snapshot_mode)
		{
			host_rtos_set_semaphore(&snapshot_semaphore, WICED_TRUE);
		}
	    //DMA2_Stream1->M0AR = (uint32_t)frame_buffer;
	    //DMA2_Stream1->NDTR = 0;
		//DMA_Cmd(DMA2_Stream1, DISABLE);
	    //DMA_Cmd(DMA2_Stream1, ENABLE);
	}

	// Przerwanie generowane przy zmianie stanu sygnalu VSYNC z aktywnego na nieaktywny (VPOL = Low)
//	if(DCMI_GetITStatus(DCMI_IT_VSYNC) == SET)
//	{
//		DCMI_ClearITPendingBit(DCMI_IT_VSYNC);
//		//while(DMA_GetFlagStatus(DMA2_Stream1, DMA_FLAG_TCIF1) == RESET);
//	}
//
//	// Przerwanie generowane przy zmianie stanu sygnalu HSYNC z aktywnego na nieaktywny (HPOL = Low)
//	if(DCMI_GetITStatus(DCMI_IT_LINE) == SET)
//	{
//		DCMI_ClearITPendingBit(DCMI_IT_LINE);
//	}
//
//	// Przerwanie generowane gdy stare dane (32-bitowe) w rejestrze DCMI_DR
//	// nie zostaly calkowicie przeslane przed nadejsciem nowych danych
//	if(DCMI_GetITStatus(DCMI_IT_OVF) == SET)
//	{
//		DCMI_ClearITPendingBit(DCMI_IT_OVF);
//	}
}

void DMA2_Stream1_irq(void)
{
	DMA_ClearITPendingBit ( DMA2_Stream1, DMA_IT_TCIF1 );
	++cycles_count;
}


