/*
 * Copyright 2014, Murata Americas
 * All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Broadcom Corporation;
 * the contents of this file may not be disclosed to third parties, copied
 * or duplicated in any form, in whole or in part, without the prior
 * written permission of Broadcom Corporation.
 */

/** @file
 * Defines board support package for SN82xx EVAL board
 */
#include "platform.h"
#include "platform_config.h"
#include "platform_init.h"
#include "platform_isr.h"
#include "platform_peripheral.h"
#include "wwd_platform_common.h"
#include "wwd_rtos.h"
#include "wiced_defaults.h"
#include "wiced_platform.h"
#include "cpal_i2c.h"
#include "platform_ext_memory.h"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/
extern wiced_result_t ext_memory_configure();
/******************************************************
 *               Variables Definitions
 ******************************************************/

/* GPIO pin table. Used by WICED/platform/MCU/wiced_platform_common.c */
const platform_gpio_t platform_gpio_pins[] =
{
    [WICED_GPIO_1]  = {GPIOA,  5}, //ADC1_IN5
    [WICED_GPIO_2]  = {GPIOC,  1}, //ADC2_IN11
    [WICED_GPIO_3]  = {GPIOC,  2}, //ADC3_IN12
    [WICED_GPIO_4]  = {GPIOA,  4}, //DCMI_HSYNC
    [WICED_GPIO_5]  = {GPIOA,  5}, //SPI1_SCK
    [WICED_GPIO_6]  = {GPIOA,  6}, //DCMI_PIXCK
    [WICED_GPIO_7]  = {GPIOA,  7}, //SPI1_MOSI
    [WICED_GPIO_8]  = {GPIOA,  8}, //RCC_MCO1

    [WICED_GPIO_9]  = {GPIOB,  3},
    [WICED_GPIO_10] = {GPIOB,  4}, //SPI1_MISO
    [WICED_GPIO_11] = {GPIOB,  6}, //DCMI_D5
    [WICED_GPIO_12] = {GPIOB,  7}, //DCMI_VSYNC
    [WICED_GPIO_15] = {GPIOB, 12}, //RST_N
    [WICED_GPIO_16] = {GPIOB, 13}, //GPIO_0
    [WICED_GPIO_17] = {GPIOB, 14}, //GPIO_1

    [WICED_GPIO_18] = {GPIOC,  6}, //DCMI_D0
    [WICED_GPIO_19] = {GPIOC,  7}, //DCMI_D1
    [WICED_GPIO_20] = {GPIOC,  8}, //SDIO_D0
    [WICED_GPIO_21] = {GPIOB,  5}, //SDIO_D1/SPI1_IRQ
    [WICED_GPIO_22] = {GPIOC, 10}, //SDIO_D2
    [WICED_GPIO_23] = {GPIOB,  3}, //SDIO_D3/SPI1_CS
    [WICED_GPIO_24] = {GPIOC, 12}, //SDIO_CK
    [WICED_GPIO_25] = {GPIOA,  0}, //LIMIT_SWITCH1

    [WICED_GPIO_26] = {GPIOD,  2}, //SDIO_CMD
    [WICED_GPIO_27] = {GPIOA, 2}, //USART2_TX
    [WICED_GPIO_28] = {GPIOA, 3}, //USART2_RX

    [WICED_GPIO_29] = {GPIOE,  0}, //DCMI_D2
    [WICED_GPIO_30] = {GPIOE,  1}, //DCMI_D3
    [WICED_GPIO_31] = {GPIOE,  4}, //DCMI_D4
    [WICED_GPIO_32] = {GPIOE,  5}, //DCMI_D6
    [WICED_GPIO_33] = {GPIOE,  6}, //DCMI_D7
    [WICED_GPIO_34] = {GPIOB, 15}, //VDD_3V3_EN
    [WICED_GPIO_35] = {GPIOA, 1}, //TIM2_CH2
    [WICED_GPIO_36] = {GPIOB, 1}, //INT

    [WICED_GPIO_13] = {CPAL_I2C1_SCL_GPIO_PORT, CPAL_I2C1_SCL_GPIO_PINSOURCE}, //I2C1_SCL
    [WICED_GPIO_14] = {CPAL_I2C1_SDA_GPIO_PORT, CPAL_I2C1_SDA_GPIO_PINSOURCE}, //I2C1_SDA

    [WICED_GPIO_37] = {CPAL_I2C2_SCL_GPIO_PORT, CPAL_I2C2_SCL_GPIO_PINSOURCE}, //I2C2_SCL
    [WICED_GPIO_38] = {CPAL_I2C2_SDA_GPIO_PORT, CPAL_I2C2_SDA_GPIO_PINSOURCE}, //I2C2_SDA

    [WICED_GPIO_39] = {GPIOD,  12}, //GREEN_LED
    [WICED_GPIO_40] = {GPIOD,  13}, //ORANGE_LED
    [WICED_GPIO_41] = {GPIOD,  14}, //RED_LED
    [WICED_GPIO_42] = {GPIOD,  15}, //BLUE_LED
};

/* ADC peripherals. Used WICED/platform/MCU/wiced_platform_common.c */
const platform_adc_t platform_adc_peripherals[] =
{
    [WICED_ADC_1] = {ADC1, ADC_Channel_5, RCC_APB2Periph_ADC1, 1, &platform_gpio_pins[WICED_GPIO_1]},
    [WICED_ADC_2] = {ADC2, ADC_Channel_11, RCC_APB2Periph_ADC2, 1, &platform_gpio_pins[WICED_GPIO_2]},
    [WICED_ADC_3] = {ADC3, ADC_Channel_12, RCC_APB2Periph_ADC3, 1, &platform_gpio_pins[WICED_GPIO_3]}
};

/* PWM peripherals. Used by WICED/platform/MCU/wiced_platform_common.c */
const platform_pwm_t platform_pwm_peripherals[] =
{
    [WICED_PWM_1]  = {TIM2, 2, RCC_APB1Periph_TIM2, GPIO_AF_TIM2, &platform_gpio_pins[WICED_GPIO_35]}
};

/* PWM peripherals. Used by WICED/platform/MCU/wiced_platform_common.c */
const platform_spi_t platform_spi_peripherals[] =
{
    [WICED_SPI_1]  =
    {
        .port                  = SPI1,
        .gpio_af               = GPIO_AF_SPI1,
        .peripheral_clock_reg  = RCC_APB2Periph_SPI1,
        .peripheral_clock_func = RCC_APB2PeriphClockCmd,
        .pin_mosi              = &platform_gpio_pins[WICED_GPIO_8],
        .pin_miso              = &platform_gpio_pins[WICED_GPIO_7],
        .pin_clock             = &platform_gpio_pins[WICED_GPIO_6],
        .tx_dma =
        {
            .controller        = DMA2,
            .stream            = DMA2_Stream5,
            .channel           = DMA_Channel_3,
            .irq_vector        = DMA2_Stream5_IRQn,
            .complete_flags    = DMA_HISR_TCIF5,
            .error_flags       = ( DMA_HISR_TEIF5 | DMA_HISR_FEIF5 | DMA_HISR_DMEIF5 ),
        },
        .rx_dma =
        {
            .controller        = DMA2,
            .stream            = DMA2_Stream0,
            .channel           = DMA_Channel_3,
            .irq_vector        = DMA2_Stream0_IRQn,
            .complete_flags    = DMA_LISR_TCIF0,
            .error_flags       = ( DMA_LISR_TEIF0 | DMA_LISR_FEIF0 | DMA_LISR_DMEIF0 ),
        },
    }
};

CPAL_TransferTypeDef cpal_transfer_rx_struct[WICED_I2C_MAX];
CPAL_TransferTypeDef cpal_transfer_tx_struct[WICED_I2C_MAX];
host_semaphore_type_t cpal_transfer_semaphore[WICED_I2C_MAX];

const platform_i2c_t platform_i2c_peripherals[] =
{
    [WICED_I2C_1] =
    {
        //.pin_scl                 = &platform_gpio_pins[WICED_GPIO_13],
        //.pin_sda                 = &platform_gpio_pins[WICED_GPIO_14],
        /*.peripheral_clock_reg    = RCC_APB1Periph_I2C1,
        .tx_dma                  = DMA1,
        .tx_dma_peripheral_clock = RCC_AHB1Periph_DMA1,
        .tx_dma_stream           = DMA1_Stream7,
        .rx_dma_stream           = DMA1_Stream5,
        .tx_dma_stream_id        = 7,
        .rx_dma_stream_id        = 5,
        .tx_dma_channel          = DMA_Channel_1,
        .rx_dma_channel          = DMA_Channel_1,
        .gpio_af                 = GPIO_AF_I2C1*/

        .cpal_i2c                = CPAL_I2C1,
        .cpal_i2c_struct         = &I2C1_DevStructure,
        .cpal_transfer_rx_struct = &cpal_transfer_rx_struct[WICED_I2C_1],
        .cpal_transfer_tx_struct = &cpal_transfer_tx_struct[WICED_I2C_1],
        .cpal_transfer_semaphore = &cpal_transfer_semaphore[WICED_I2C_1],
    },

    [WICED_I2C_2] =
    {
        //.pin_scl                 = &platform_gpio_pins[WICED_GPIO_37],
        //.pin_sda                 = &platform_gpio_pins[WICED_GPIO_38],
        /*.peripheral_clock_reg    = RCC_APB1Periph_I2C1,
        .tx_dma                  = DMA1,
        .tx_dma_peripheral_clock = RCC_AHB1Periph_DMA1,
        .tx_dma_stream           = DMA1_Stream7,
        .rx_dma_stream           = DMA1_Stream5,
        .tx_dma_stream_id        = 7,
        .rx_dma_stream_id        = 5,
        .tx_dma_channel          = DMA_Channel_1,
        .rx_dma_channel          = DMA_Channel_1,
        .gpio_af                 = GPIO_AF_I2C1*/

        .cpal_i2c                = CPAL_I2C2,
        .cpal_i2c_struct         = &I2C2_DevStructure,
        .cpal_transfer_rx_struct = &cpal_transfer_rx_struct[WICED_I2C_2],
        .cpal_transfer_tx_struct = &cpal_transfer_tx_struct[WICED_I2C_2],
        .cpal_transfer_semaphore = &cpal_transfer_semaphore[WICED_I2C_2],
    },
};

/* UART peripherals and runtime drivers. Used by WICED/platform/MCU/wiced_platform_common.c */
const platform_uart_t platform_uart_peripherals[] =
{
    /*[WICED_UART_1] =
    {
        .port               = USART3,
        .tx_pin             = &platform_gpio_pins[WICED_GPIO_27 ],
        .rx_pin             = &platform_gpio_pins[WICED_GPIO_28],
        .cts_pin            = NULL,
        .rts_pin            = NULL,
        .tx_dma_config =
        {
            .controller     = DMA1,
            .stream         = DMA1_Stream3,
            .channel        = DMA_Channel_4,
            .irq_vector     = DMA1_Stream3_IRQn,
            .complete_flags = DMA_LISR_TCIF3,
            .error_flags    = ( DMA_LISR_TEIF3 | DMA_LISR_FEIF3 | DMA_LISR_DMEIF3 ),
        },
        .rx_dma_config =
        {
            .controller     = DMA1,
            .stream         = DMA1_Stream1,
            .channel        = DMA_Channel_4,
            .irq_vector     = DMA1_Stream1_IRQn,
            .complete_flags = DMA_LISR_TCIF1,
            .error_flags    = ( DMA_LISR_TEIF1 | DMA_LISR_FEIF1 | DMA_LISR_DMEIF1 ),
        },
    },*/
    [WICED_UART_1] =
    {
        .port               = USART2,
        .tx_pin             = &platform_gpio_pins[WICED_GPIO_27],
        .rx_pin             = &platform_gpio_pins[WICED_GPIO_28],
        .cts_pin            = NULL,
        .rts_pin            = NULL,
        .tx_dma_config =
        {
            .controller     = DMA1,
            .stream         = DMA1_Stream6,
            .channel        = DMA_Channel_4,
            .irq_vector     = DMA1_Stream6_IRQn,
            .complete_flags = DMA_HISR_TCIF6,
            .error_flags    = ( DMA_HISR_TEIF6 | DMA_HISR_FEIF6 | DMA_HISR_DMEIF6 ),
        },
        .rx_dma_config =
        {
            .controller     = DMA1,
            .stream         = DMA1_Stream5,
            .channel        = DMA_Channel_4,
            .irq_vector     = DMA1_Stream5_IRQn,
            .complete_flags = DMA_HISR_TCIF5,
            .error_flags    = ( DMA_HISR_TEIF5 | DMA_HISR_FEIF5 | DMA_HISR_DMEIF5 ),
        },
    },
};
platform_uart_driver_t platform_uart_drivers[WICED_UART_MAX];

/* SPI flash. Exposed to the applications through include/wiced_platform.h */
#if defined ( WICED_PLATFORM_INCLUDES_SPI_FLASH )
const wiced_spi_device_t wiced_spi_flash =
{
	.port        = WICED_SPI_1,
	.chip_select = WICED_SPI_FLASH_CS,
    .speed       = 5000000,
	.mode        = (SPI_CLOCK_RISING_EDGE | SPI_CLOCK_IDLE_HIGH | SPI_NO_DMA | SPI_MSB_FIRST),
	.bits        = 8
};
#endif

const wiced_i2c_device_t wiced_i2c_camera =
{
    .port = WICED_I2C_1
};

const wiced_i2c_device_t wiced_i2c_imu =
{
    .port = WICED_I2C_2
};

/* UART standard I/O configuration */
#ifndef WICED_DISABLE_STDIO
static platform_uart_config_t stdio_config =
{
    .baud_rate    = 115200,
    .data_width   = DATA_WIDTH_8BIT,
    .parity       = NO_PARITY,
    .stop_bits    = STOP_BITS_1,
    .flow_control = FLOW_CONTROL_DISABLED,
};
#endif

/* Wi-Fi control pins. Used by WICED/platform/MCU/wwd_platform_common.c
 * SDIO: WWD_PIN_BOOTSTRAP[1:0] = b'00
 * gSPI: WWD_PIN_BOOTSTRAP[1:0] = b'01
 */
const platform_gpio_t wifi_control_pins[] =
    {
    [WWD_PIN_POWER      ] = { GPIOB, 15 },
    [WWD_PIN_RESET      ] = { GPIOB, 12 },
#if defined ( WICED_USE_WIFI_32K_CLOCK_MCO )
    [WWD_PIN_32K_CLK    ] = { GPIOA,  8 },
#else
    [WWD_PIN_32K_CLK    ] = { GPIOA, 11 },
#endif
    [WWD_PIN_BOOTSTRAP_0] = { GPIOC, 4 },
    [WWD_PIN_BOOTSTRAP_1] = { GPIOC, 5 },
};

/* Wi-Fi SDIO bus pins. Used by WICED/platform/STM32F2xx/WWD/wwd_SDIO.c */
const platform_gpio_t wifi_sdio_pins[] =
{
    [WWD_PIN_SDIO_OOB_IRQ] = { GPIOB, 13 },
    [WWD_PIN_SDIO_CLK    ] = { GPIOC, 12 },
    [WWD_PIN_SDIO_CMD    ] = { GPIOD,  2 },
    [WWD_PIN_SDIO_D0     ] = { GPIOC,  8 },
    [WWD_PIN_SDIO_D1     ] = { GPIOC,  9 },
    [WWD_PIN_SDIO_D2     ] = { GPIOC, 10 },
    [WWD_PIN_SDIO_D3     ] = { GPIOC, 11 },
};

/* Wi-Fi gSPI bus pins. Used by WICED/platform/STM32F2xx/WWD/wwd_SPI.c */
const platform_gpio_t wifi_spi_pins[] =
{
    [WWD_PIN_SPI_IRQ ] = { GPIOB,  5 },
    [WWD_PIN_SPI_CS  ] = { GPIOB,  3 },
    [WWD_PIN_SPI_CLK ] = { GPIOB,  13 },
    [WWD_PIN_SPI_MOSI] = { GPIOC,  3 },
    [WWD_PIN_SPI_MISO] = { GPIOB,  14 },
};

/******************************************************
 *               Function Definitions
 ******************************************************/

void platform_init_peripheral_irq_priorities( void )
{
    /* Interrupt priority setup. Called by WICED/platform/MCU/STM32F2xx/platform_init.c */
    NVIC_SetPriority( RTC_WKUP_IRQn    ,  1 ); /* RTC Wake-up event   */
    NVIC_SetPriority( SDIO_IRQn        ,  2 ); /* WLAN SDIO           */
    NVIC_SetPriority( DMA2_Stream3_IRQn,  3 ); /* WLAN SDIO DMA       */
    NVIC_SetPriority( DMA1_Stream3_IRQn,  3 ); /* WLAN SPI DMA        */
    NVIC_SetPriority( DCMI_IRQn,		  4 );
    NVIC_SetPriority( DMA2_Stream1_IRQn,  4 );
    NVIC_SetPriority( USART2_IRQn      ,  6 ); /* WICED_UART_2        */
    NVIC_SetPriority( DMA1_Stream6_IRQn,  7 ); /* WICED_UART_2 TX DMA */
    NVIC_SetPriority( DMA1_Stream5_IRQn,  7 ); /* WICED_UART_2 RX DMA */
    NVIC_SetPriority( EXTI0_IRQn       , 8 ); /* GPIO                */
    NVIC_SetPriority( EXTI1_IRQn       , 8 ); /* GPIO                */
    NVIC_SetPriority( EXTI2_IRQn       , 8 ); /* GPIO                */
    NVIC_SetPriority( EXTI3_IRQn       , 8 ); /* GPIO                */
    NVIC_SetPriority( EXTI4_IRQn       , 8 ); /* GPIO                */
    NVIC_SetPriority( EXTI9_5_IRQn     , 8 ); /* GPIO                */
    NVIC_SetPriority( EXTI15_10_IRQn   , 8 ); /* GPIO                */
}

void platform_init_external_devices( void )
    {
#ifndef WICED_DISABLE_STDIO
    /* Initialise UART standard I/O */
    platform_stdio_init( &platform_uart_drivers[STDIO_UART], &platform_uart_peripherals[STDIO_UART], &stdio_config );
#endif
    }

/******************************************************
 *           Interrupt Handler Definitions
 ******************************************************/

WWD_RTOS_DEFINE_ISR( usart2_irq )
    {
    platform_uart_irq( &platform_uart_drivers[WICED_UART_1] );
    }

WWD_RTOS_DEFINE_ISR( usart2_tx_dma_irq )
{
    platform_uart_tx_dma_irq( &platform_uart_drivers[WICED_UART_1] );
}

WWD_RTOS_DEFINE_ISR( usart2_rx_dma_irq )
    {
    platform_uart_rx_dma_irq( &platform_uart_drivers[WICED_UART_1] );
    }

/******************************************************
 *            Interrupt Handlers Mapping
 ******************************************************/

/* These DMA assignments can be found STM32F2xx datasheet DMA section */
WWD_RTOS_MAP_ISR( usart2_irq       , USART2_irq       )
WWD_RTOS_MAP_ISR( usart2_tx_dma_irq, DMA1_Stream6_irq )
WWD_RTOS_MAP_ISR( usart2_rx_dma_irq, DMA1_Stream5_irq )

void platform_init_memory()
{
	ext_memory_configure();
}
