--------------------------------------------
SN8000xUFL - README
--------------------------------------------

Provider    : Murata 
Website     : http://www.murata-ws.com/wi-fi-network-controller.htm
Description : Murata SN8205 mounted on SN82XX evaluation board 
 
Module
  Mfr     : Murata
  P/N     : 88-00158-00
  MCU     : STM32F205RGT6
  WLAN    : BCM43362
  Antenna : on-board antenna

EVB Features
  JLINK-JTAG debug interface
  USB-JTAG debug interface
  USB-serial UART interface
  Power supply : USB and/or external +5v
  Reset button
  Module current monitor
  Sensors/Peripherals
     - 1 x SHT21 temerature and relative humidity sensor
     - 1 x 8Mbit serial flash
  Pads for 24-pin expansion header
            
