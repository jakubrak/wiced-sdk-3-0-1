--------------------------------------------
BCM94390WCD2 - README
--------------------------------------------

Provider    : Broadcom
Website     : http://broadcom.com/wiced
Description : Broadcom Internal Dev Board - BCM94390WCD1 mounted on a Broadcom BCM9WCD3EVAL1 evaluation board

Module
  Mfr     : Broadcom
  P/N     : BCM94390WCD1 Discrete board based on Dev package of 4390 
  MCU     : BCM4390 Integrated Applications processor (ARM CM3 @ 48MHz)
  WLAN    : BCM4390 Integrated Wi-Fi processor (802.11b/g/n 1x1)
  Antenna : Diversity with two printed antennae (and in-line switched Murata MM8430 RF connectors)

EVB Features
  USB-JTAG debug interface
  USB-serial UART interface
  Power supply : USB and/or external +5V
  Reset button
  Wake button
  Module current monitor
  Sensors/Peripherals
     - 2 x Buttons
     - 2 x LEDs
     - 1 x SPI Thermistor
     - 1 x 16Mbit serial flash
  30-pin Expansion header

