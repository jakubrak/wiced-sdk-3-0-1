var alive = 1;
var horn_angle = 51.3;

function reset_timer()
{
	timeoutID = setTimeout('reloadData()', 500);
}
  
function reloadData()
{
	do_ajax( "/imu.html?angle=" + horn_angle, null, update);
	if(alive);
	{
		reset_timer();
	}
}

function init()
{
	reloadData();
}

function adjust_screen(options) 
{
    options = options || {};
    var top = options.paddingTop || 0;
    var left = options.paddingLeft || 0;
    var right = options.paddingRight || 0;
    var bottom = options.paddingBottom || 0;
    if (!document.body) 
    {
		throw 'document.body doesn\'t exist yet (call gl.fullscreen() from ' +
			'window.onload() or from inside the <body> tag)';
    }
    document.getElementById("canvas").appendChild(gl.canvas);
    gl.canvas.style.position = 'relative';
    gl.canvas.style.left = left + 'px';
    gl.canvas.style.top = top + 'px';
    function resize() 
	{
		gl.canvas.width = window.innerWidth - left - right;
		gl.canvas.height = (window.innerHeight - top - bottom) * 0.5;
		gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
		if (options.camera || !('camera' in options))
		{
			gl.matrixMode(gl.PROJECTION);
			gl.loadIdentity();
			gl.perspective(options.fov || 45, gl.canvas.width / gl.canvas.height,
				options.near || 0.1, options.far || 1000);
			gl.matrixMode(gl.MODELVIEW);
		}
		if (gl.ondraw)
			gl.ondraw();
	};
	window.addEventListener("resize", resize);
	resize();
}

function update(response) 
{
	var quat = new Float32Array(response);
	document.getElementById('quaternion').innerHTML = "[" + quat[0].toFixed(3) + ", " + quat[1].toFixed(3) + ", " + quat[2].toFixed(3) + ", " + quat[3].toFixed(3) + "]"
	document.getElementById('hornangle').innerHTML  = horn_angle + "&deg";
	/*if(gl)
	{
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
		gl.clearColor(1.0, 1.0, 1.0, 1.0);
		gl.loadIdentity();
		gl.translate(0, 0, -4);
		gl.rotate(2.0*Math.acos(quat[0])*180.0/Math.PI, quat[1], quat[3], quat[2]);
		gl.pushMatrix();
		gl.pushMatrix();
		gl.rotate(horn_angle, 0, 1, 0);
		shader.draw(mesh3);
		gl.popMatrix();
		shader.draw(mesh2);
		gl.popMatrix();
	}*/
};

function do_ajax( ajax_url, data_to_send, receive_func )
{
	req = null;
	if (window.XMLHttpRequest)
	{
		req = new XMLHttpRequest();
	}
	else if (window.ActiveXObject)
	{
		try 
		{
			req = new ActiveXObject("Msxml2.XMLHTTP");
	    } 
	    catch (e)
	    {
			try 
			{
	        	req = new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch (e) 
			{
		        alert("Your browser does not support AJAX - please use a different browser.");
		        return;
	      	}
	    }
	}
	req.responseType = 'arraybuffer';
	
	req.onreadystatechange = function()
	{
		if(req.readyState == 4)
		{
			if(req.status == 200)
			{
				if(receive_func != null)
				{
					receive_func(req.response);
				}
			}
			else
			{
				alive = 0;
				document.getElementById("status").innerHTML = "Connection lost";
			}
		}
	};
  	req.open("GET", ajax_url, true);
  	req.send(null);
}

