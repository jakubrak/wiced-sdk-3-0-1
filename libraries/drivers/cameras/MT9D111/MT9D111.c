#include "MT9D111.h"
#include "wiced.h"

extern int camera_i2c_read_reg(unsigned char reg_addr, unsigned short *reg_val);
extern int camera_i2c_write_reg(unsigned char reg_addr, unsigned short reg_val);
extern void camera_delay_ms(unsigned long ms);

struct MT9D111_REG_STRUCT MT9D111_InitStructure[] = {
	//***************************
	// Fin  = 8  MHz
	// Fpfd = 8  MHz
	// Fvco = 200 MHz
	// Fout = 50 MHz
	//***************************

	{0xF0, 0x0000}, //Select page 0
	{0x65, 0xA000},  //Clock: <15> PLL BYPASS = 1 --- make sure that PLL is bypassed
	{0x65, 0xE000},  //Clock: <14> PLL OFF = 1 --- make sure that PLL is powered-down
	{0x66, 0x1900}, //PLL Control 1: <15:8> M = 25, <5:0> N = 0
	{0x67, 0x0501}, //PLL Control 2: <6:0> P = 4
	{0x65, 0xA000}, //Clock: <14> PLL OFF = 0 --- PLL is powered-up
	{0x65, 0x2000}, //Clock: <15> PLL BYPASS = 0 --- enable PLL as master clock

	{0xF0, 0x0000}, //Select page 0
	{0x01, 0x001C},  //Row Start = 28
	{0x02, 0x002C},  //Column Start = 44
	{0x03, 0x04B0},  //Row Width = 1200
	{0x04, 0x0640},  //Column Width = 1600

	{0xF0, 0x0001}, //Select page 1
	{0x08, 0x01F8},  //Decimator enabled
	{0x12, 0x0640},  //X1 Coordinate for Crop Window +1; width = 1600
	{0x14, 0x04B0},  //Y1 Coordinate for Crop Window +1; height = 1200
	{0x16, 0x019A},  //Weight for H Decimation = 410
	{0x17, 0x019A},  //Weight for V Decimation = 410

	//******************************************
	// Konfiguracja parametrow wykonania obrazu
	//******************************************
	{0xF0, 0x0000}, //Select page 0
	{0x09, 0x02BC},  //Shutter Width = 700 --- Ustawienie wartosci przeslony
	{0x20, 0x0300},  //Read Mode (B): binning disabled, use both ADCs, normal UXGA size, column/row skip disabled
	{0x21, 0x0000},  //Read Mode (A): binning disabled, use both ADCs, column/row skip disabled

	//**********************************************
	// Konfiguracja formatu obrazu wyjsciowego
	//**********************************************
	{0xF0, 0x0001}, //Select page 1
	{0x09, 0x0009},  //Factory bypass: Data from SOC going to Dout pads
	{0x97, 0x0022},  //Output Format Config: RGB output, swap odd/even bytes

	{0xFF, 0xFFFF}
};

int MT9D111_SetRegs(struct MT9D111_REG_STRUCT *reglist)
{
	const struct MT9D111_REG_STRUCT *next = reglist;
	uint16_t current_page;
	uint16_t read_value;

	//WPRINT_LIB_DEBUG(("Start configuring MT9D111 camera...\r\n"));
	while (next->address != 0xFF || next->value != 0xFFFF)
	{
		if(next->address == 0xF0)
		{
			current_page = next->value;
		}

		camera_i2c_write_reg(next->address, next->value);

		camera_i2c_read_reg(next->address, &read_value);
		if(read_value != next->value)
		{
			WPRINT_LIB_DEBUG(("Operation R0x%x:%d=0x%x failed (read value: %d)\r\n",
					next->address, current_page, next->value, read_value));
		}
		++next;
	}
	return 0;
}

void MT9D111_Init()
{
	MT9D111_SetRegs(MT9D111_InitStructure);

	camera_delay_ms(1);
	camera_i2c_write_reg(0xC6, 0xA103);
	camera_i2c_write_reg(0xC8, 0x06);
	camera_delay_ms(1);
	camera_i2c_write_reg(0xC6, 0xA103);
	camera_i2c_write_reg(0xC8, 0x05);
	camera_delay_ms(100);

	camera_i2c_write_reg(0xC6, 0xA103);
	camera_i2c_write_reg(0xC8, 0x02);
}

/** Soft Reset
  * 1. Bypass the PLL, R0x65:0=0xA000, if it is currently used
  * 2. Perform MCU reset by setting R0xC3:1=0x0501
  * 3. Enable soft reset by setting R0x0D:0=0x0021. Bit 0 is used for
  *    the sensor core reset while bit 5 refers to SOC reset.
  * 4. Disable soft reset by setting R0x0D:0=0x0000
  * 5. Wait 24 clock cycles before using the two-wire serial interface
  */
void MT9D111_SoftReset(){
	camera_i2c_write_reg(PAGE_SELECT_REG , 0x0   ); // PAGE 0
	camera_i2c_write_reg(CLOCK_REG       , 0xA000); // Bypass the PLL
	camera_i2c_write_reg(PAGE_SELECT_REG , 0x1   ); // PAGE 1
	camera_i2c_write_reg(UC_BOOT_MODE_REG, 0x0501); // Perform MCU Reset
	camera_i2c_write_reg(UC_BOOT_MODE_REG, 0x0500);
	camera_i2c_write_reg(PAGE_SELECT_REG , 0x0   ); // PAGE 0
	camera_i2c_write_reg(RESET_REG       , 0x0021); // Enable Soft Reset
	camera_i2c_write_reg(RESET_REG       , 0x0000); // Disable Soft Reset
	camera_delay_ms(10);                                   // Wait 10 ms
}
