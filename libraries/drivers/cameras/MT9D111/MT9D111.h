#pragma once

#define MT9D111_DEVICE_ADDRESS  0x5D

struct MT9D111_REG_STRUCT
{
	unsigned char address;
	unsigned short value;
};

void MT9D111_Init();
void MT9D111_SoftReset();

/* Register names ------------------------------------------------------------*/
/* Page 0 --- Sensor registers */
#define ROW_START_REG			0x01
#define COL_START_REG			0x02
#define ROW_WIDTH_REG			0x03
#define COL_WIDTH_REG			0x04
#define H_BLANKING_CTX_B_REG	0x05
#define V_BLANKING_CTX_B_REG	0x06
#define H_BLANKING_CTX_A_REG	0x07
#define V_BLANKING_CTX_A_REG	0x08
#define SHUTTER_WIDTH_REG		0x09
#define ROW_SPEED_REG			0x0A
#define EXTRA_DELAY_REG			0x0B
#define SHUTTER_DELAY_REG		0x0C
#define RESET_REG				0x0D
#define FRAME_VALID_CTRL_REG	0x1F
#define READ_MODE_CTX_B_REG		0x20
#define READ_MODE_CTX_A_REG		0x21
#define SHOW_CTRL_REG			0x22
#define FLASH_CTRL_REG			0x23
#define EXTRA_RESET_REG			0x24
#define LINE_VALID_CTRL_REG		0x25
#define BOTTOM_DARK_ROWS_REG	0x26
#define GREEN1_GAIN_REG			0x2B
#define BLUE_GAIN_REG			0x2C
#define RED_GAIN_REG			0x2D
#define GREEN2_GAIN_REG			0x2E
#define GLOBAL_GAIN_REG			0x2F
#define ROW_NOISE_REG			0x30
#define BLACK_ROWS_REG			0x59
#define GREEN1_FRAME_AVRG_REG	0x5B
#define BLUE_FRAME_AVRG_REG		0x5C
#define RED_FRAME_AVRG_REG		0x5D
#define GREEN2_FRAME_AVRG_REG	0x5E
#define THRESHOLD_REG			0x5F
#define CALIBRATION_CTRL_REG	0x60
#define GREEN1_CALIBR_VALUE_REG	0x61
#define BLUE_CALIBR_VALUE_REG	0x62
#define RED_CALIBR_VALUE_REG	0x63
#define GREEN2_CALIBR_VALUE_REG	0x64
#define CLOCK_REG				0x65
#define PLL_CTRL1_REG			0x66
#define PLL_CTRL2_REG			0x67
#define GLOBAL_RESET_CTRL_REG	0xC0
#define START_INT_TIME_REG		0xC1
#define START_READOUT_TIME_REG	0xC2
#define ASSERT_STRB_TIME_REG	0xC3
#define DEASSERT_STRB_TIME_REG	0xC4
#define ASSERT_FLASH_TIME_REG	0xC5
#define DEASSERT_FLASH_TIME_REG	0xC6
#define AIN3_SAMPLE_REG			0xE0
#define AIN2_SAMPLE_REG			0xE1
#define AIN1_SAMPLE_REG			0xE2
#define EXT_SIGN_SAMP_CTRL_REG	0xE3
#define PAGE_SELECT_REG			0xF0
#define BYTEWISE_ADRR_REG		0xF1
#define CTX_CTRL_REG			0xF2

/* Page 1 - IFP Registers */
#define COLOR_PIPELINE_CTRL_REG 0x08
#define FACTORY_BYPASS_REG      0x09
#define PAD_SLEW_REG            0x0A
#define INTERNAL_CLOCK_CTRL_REG 0x0B
#define X0_COORD_CROP_WIN_REG   0x11
#define X1_COORD_CROP_WIN_REG   0x12
#define Y0_COORD_CROP_WIN_REG   0x13
#define Y1_COORD_CROP_WIN_REG   0x14
#define DECIMATOR_CTRL_REG      0x21
#define WEIGHT_H_DECIMATION_REG 0x16
#define WEIGHT_V_DECIMATION_REG 0x17
//...
#define TEST_PATTERN_REG        0x48
#define TEST_PATTERN_R_REG      0x49
#define TEST_PATTERN_G_REG      0x4A
#define TEST_PATTERN_B_REG      0x4B
//...
#define OUTPUT_FORMAT_CONF_REG  0x97
#define OUTPUT_FORMAT_TEST_REG  0x98
#define LINE_COUNT_REG          0x99
#define FRAME_COUNT_REG         0x9A
#define SPEC_EFFECTS_REG        0xA4
//...
#define UC_BOOT_MODE_REG        0xC3
#define UC_VAR_ADDR_REG         0xC6
#define UC_VAR_DATA_REG         0xC8
//...

/* Page 2 - IFP Registers */
//...
