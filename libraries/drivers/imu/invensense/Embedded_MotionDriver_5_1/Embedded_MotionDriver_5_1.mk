#
# Copyright 2014, Broadcom Corporation
# All Rights Reserved.
#
# This is UNPUBLISHED PROPRIETARY SOURCE CODE of Broadcom Corporation;
# the contents of this file may not be disclosed to third parties, copied
# or duplicated in any form, in whole or in part, without the prior
# written permission of Broadcom Corporation.
#

NAME := Embedded_MotionDriver_5_1

$(NAME)_SOURCES := inv_mpu.c \
				   inv_mpu_dmp_motion_driver.c
				   
GLOBAL_INCLUDES := .

$(NAME)_CFLAGS := -Wno-error=unused-variable \
                  -Wno-error=unused-function \
                  -Wno-error=unused-value \
                  -Wno-error=uninitialized

