var group__tcp =
[
    [ "TCP packet comms", "group__tcppkt.html", "group__tcppkt" ],
    [ "TCP buffer comms", "group__tcpbfr.html", "group__tcpbfr" ],
    [ "TCP stream comms", "group__tcpstream.html", "group__tcpstream" ],
    [ "wiced_tcp_accept", "group__tcp.html#gaf0328a5cb53fde8d32ec0341271b3ef6", null ],
    [ "wiced_tcp_bind", "group__tcp.html#gaca7d00f4aad7cf2c47cf32996803def1", null ],
    [ "wiced_tcp_connect", "group__tcp.html#ga51aa9df02b62a94edad1643e635e42a3", null ],
    [ "wiced_tcp_create_socket", "group__tcp.html#ga7e872af55b2a7a3bdf84bf0dfcae19d2", null ],
    [ "wiced_tcp_delete_socket", "group__tcp.html#gadbf49726f7962e9b5142f9a9d9a50229", null ],
    [ "wiced_tcp_disconnect", "group__tcp.html#ga2c022288f4f412250189eb73f4591e78", null ],
    [ "wiced_tcp_enable_tls", "group__tcp.html#gaa8bbc73b48f9fd23bf92324e83dbe655", null ],
    [ "wiced_tcp_listen", "group__tcp.html#ga6954d52ed68db6db3df9872926ff301c", null ],
    [ "wiced_tcp_register_callbacks", "group__tcp.html#gad59ccebd26e3ff2e7deb656a1710a821", null ],
    [ "wiced_tcp_server_peer", "group__tcp.html#ga77cc2a7ed9597cba008cdf178b9703df", null ],
    [ "wiced_tcp_start_tls", "group__tcp.html#gae25065d3b56d32fef81c175babc2274a", null ]
];