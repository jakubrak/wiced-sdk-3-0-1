/*
 * Copyright 2014, Broadcom Corporation
 * All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Broadcom Corporation;
 * the contents of this file may not be disclosed to third parties, copied
 * or duplicated in any form, in whole or in part, without the prior
 * written permission of Broadcom Corporation.
 */

#ifndef INCLUDED_WICED_BOOTLOADER_H_
#define INCLUDED_WICED_BOOTLOADER_H_

#include <stdint.h>
#include "platform_dct.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
typedef struct update_history_elem_struct
{

} update_history_elem_t;
*/


#ifdef __cplusplus
} /*extern "C" */
#endif

#endif /* ifndef INCLUDED_WICED_BOOTLOADER_H_ */
