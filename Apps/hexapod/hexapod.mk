NAME := App_Hexapod

$(NAME)_SOURCES := hexapod.c \
				   camera.c \
				   imu.c

$(NAME)_INCLUDES := Apps/hexapod

$(NAME)_COMPONENTS := daemons/gedday \
					  drivers/imu/invensense/Embedded_MotionDriver_5_1 \
					  drivers/cameras/MT9D111 \
					  drivers/lcds/HY32C

WIFI_CONFIG_DCT_H := wifi_config_dct.h
                      
#GLOBAL_DEFINES += CPAL_DEBUG
GLOBAL_DEFINES += WPRINT_ENABLE_PLATFORM_DEBUG
GLOBAL_DEFINES += DEBUG
GLOBAL_DEFINES += MPU6050