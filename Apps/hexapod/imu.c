#include "imu.h"
#include "wiced.h"
#include "inv_mpu.h"
#include "inv_mpu_dmp_motion_driver.h"
#include "task.h"

#define IMU_THREAD_PRIORITY (WICED_APPLICATION_PRIORITY)
#define IMU_STACK_SIZE (5000) /* printf requires 4K of stack */
#define IMU_TIMEOUT 1000

// Sensor sensitivities
#define ACCEL_SENSIVITY 16384.0
#define GYRO_SENSIVITY 16.375
#define QUAT_SENSIVITY 1073741824.0

static void imu_irq_handler(void* arg);
static void imu_thread_main(uint32_t arg);
unsigned short inv_orientation_matrix_to_scalar(const signed char *mtx);
unsigned short inv_row_2_scale(const signed char *row);

signed char gyro_orientation[9] = {1, 0, 0, 0, 1, 0, 0, 0, 1};
wiced_bool_t imu_initiatlized = WICED_FALSE;
imu_data_t imu_data;
wiced_mutex_t imu_mutex;
wiced_semaphore_t imu_semaphore;

void i2c_init( void )
{
    wiced_i2c_init(&wiced_i2c_imu);
}

int i2c_read(unsigned char slave_addr, unsigned char reg_addr, unsigned char length, unsigned char *data)
{
    wiced_i2c_message_t message;
    wiced_i2c_init_rx_message(&message, data, length, 0 , WICED_FALSE);
    message.slave_address = (uint16_t)slave_addr << 1;
    message.register_address = (uint16_t)reg_addr;
    if(wiced_i2c_transfer(&wiced_i2c_imu, &message, 1) != WICED_SUCCESS)
    {
        WPRINT_APP_DEBUG(("I2C semaphore timeout\r\n"));
        return WICED_ERROR;
    }
    return WICED_SUCCESS;
}

int i2c_write(unsigned char slave_addr, unsigned char reg_addr, unsigned char length, unsigned char const *data)
{
    wiced_i2c_message_t message;
    wiced_i2c_init_tx_message(&message, data, length, 0 , WICED_FALSE);
    message.slave_address = (uint16_t)slave_addr << 1;
    message.register_address = (uint16_t)reg_addr;
    if(wiced_i2c_transfer(&wiced_i2c_imu, &message, 1) != WICED_SUCCESS)
    {
        WPRINT_APP_DEBUG(("I2C semaphore timeout\r\n"));
        return WICED_ERROR;
    }
    return WICED_SUCCESS;
}

void get_ms(unsigned long *ms)
{
    wiced_time_get_time(ms);
}

void delay_ms(unsigned long ms)
{
    wiced_rtos_delay_milliseconds(ms);
}

void reg_int_cb()
{
    wiced_gpio_init(WICED_GPIO_36, INPUT_PULL_UP);
    wiced_gpio_input_irq_enable(WICED_GPIO_36, IRQ_TRIGGER_FALLING_EDGE, imu_irq_handler, NULL);
}

void imu_irq_handler(void* arg)
{
    UNUSED_PARAMETER(arg);
    wiced_rtos_set_semaphore( &imu_semaphore );
}

unsigned short inv_orientation_matrix_to_scalar(const signed char *mtx)
{
    unsigned short scalar;

    /*
       XYZ  010_001_000 Identity Matrix
       XZY  001_010_000
       YXZ  010_000_001
       YZX  000_010_001
       ZXY  001_000_010
       ZYX  000_001_010
     */

    scalar = inv_row_2_scale(mtx);
    scalar |= inv_row_2_scale(mtx + 3) << 3;
    scalar |= inv_row_2_scale(mtx + 6) << 6;


    return scalar;
}

unsigned short inv_row_2_scale(const signed char *row)
{
    unsigned short b;

    if (row[0] > 0)
        b = 0;
    else if (row[0] < 0)
        b = 4;
    else if (row[1] > 0)
        b = 1;
    else if (row[1] < 0)
        b = 5;
    else if (row[2] > 0)
        b = 2;
    else if (row[2] < 0)
        b = 6;
    else
        b = 7;      // error
    return b;
}

wiced_result_t imu_init()
{
    wiced_rtos_init_semaphore(&imu_semaphore);
    wiced_rtos_init_mutex(&imu_mutex);
    imu_initiatlized = WICED_TRUE;
}

wiced_result_t imu_on()
{
	imu_init();
	imu_reset();
	WPRINT_APP_DEBUG(("IMU initialized\r\n"));
    wiced_rtos_create_thread(NULL, IMU_THREAD_PRIORITY, "IMU", imu_thread_main, IMU_STACK_SIZE, NULL);
    return WICED_SUCCESS;
}

wiced_result_t imu_reset()
{
	mpu_init();
	mpu_set_sensors(INV_XYZ_GYRO | INV_XYZ_ACCEL);
	mpu_set_sample_rate(10);
	dmp_load_motion_driver_firmware();
	dmp_set_orientation(inv_orientation_matrix_to_scalar(gyro_orientation));
	dmp_enable_feature(DMP_FEATURE_6X_LP_QUAT | DMP_FEATURE_GYRO_CAL);
	dmp_set_fifo_rate(10);
	mpu_set_dmp_state(1);
    return WICED_SUCCESS;
}

wiced_result_t imu_read(imu_data_t *data)
{
	if(imu_initiatlized)
	{
		wiced_rtos_lock_mutex( &imu_mutex );
		data->quat[0] = imu_data.quat[0];
		data->quat[1] = imu_data.quat[1];
		data->quat[2] = imu_data.quat[2];
		data->quat[3] = imu_data.quat[3];
		wiced_rtos_unlock_mutex( &imu_mutex );
	}
    return WICED_SUCCESS;
}

void imu_thread_main(uint32_t arg)
{
    while(1)
    {
        short gyro[3], accel[3], sensors;
        long quat[4];
        unsigned char more;
        unsigned long sensor_timestamp;

        wiced_rtos_get_semaphore(&imu_semaphore, NEVER_TIMEOUT);
        dmp_read_fifo(gyro, accel, quat, &sensor_timestamp, &sensors, &more);

        if (sensors & INV_WXYZ_QUAT)
        {
            wiced_rtos_lock_mutex( &imu_mutex );
            imu_data.quat[0] = ((float)quat[0])/QUAT_SENSIVITY;
            imu_data.quat[1] = ((float)quat[1])/QUAT_SENSIVITY;
            imu_data.quat[2] = ((float)quat[2])/QUAT_SENSIVITY;
            imu_data.quat[3] = ((float)quat[3])/QUAT_SENSIVITY;
            /*WPRINT_APP_DEBUG(("[IMU] [%f, %f, %f, %f]\r\n", imu_data.quat[0],
                    imu_data.quat[1], imu_data.quat[2], imu_data.quat[3]));*/
            wiced_rtos_unlock_mutex( &imu_mutex );
        }
    }
    WPRINT_APP_DEBUG(("[IMU] imu_thread_main stopped\r\n"));
}

