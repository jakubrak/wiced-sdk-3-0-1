
#include "wiced.h"
#include "imu.h"
#include "camera.h"
#include "lcd.h"

/******************************************************
 *                      Macros
 ******************************************************/

#define UDP_MAX_DATA_LENGTH         30
#define UDP_RX_TIMEOUT              1
#define IMU_READ_INTERVAL           10
#define BATTERY_READ_INTERVAL		1000
#define UDP_RX_INTERVAL             1
#define UDP_TARGET_PORT         	50007
#define UDP_TARGET_IS_BROADCAST
#define GET_UDP_RESPONSE

#define TCP_SERVER_LISTEN_PORT      50006

#ifdef UDP_TARGET_IS_BROADCAST
    #define UDP_TARGET_IP MAKE_IPV4_ADDRESS(192,168,0,255)
#else
    #define UDP_TARGET_IP MAKE_IPV4_ADDRESS(192,168,0,2)
#endif

uint16_t BG_Color = Blue;

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

typedef enum
{
	IMU_QUATERNION,
	SERVO_ANGLE,
	LIMIT_SWITCH,
	BATTERY
} packet_t;

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

static wiced_result_t tx_udp_timed_packet(void* arg);
static wiced_result_t rx_udp_packet(uint32_t timeout);
static void limit_switch_handler(void *arg);



/******************************************************
 *               Variables Definitions
 ******************************************************/

static const wiced_ip_setting_t device_init_ip_settings =
{
    INITIALISER_IPV4_ADDRESS( .ip_address, MAKE_IPV4_ADDRESS(192,168,  0,  1) ),
    INITIALISER_IPV4_ADDRESS( .netmask,    MAKE_IPV4_ADDRESS(255,255,255,  0) ),
    INITIALISER_IPV4_ADDRESS( .gateway,    MAKE_IPV4_ADDRESS(192,168,  0,  1) ),
};

static wiced_udp_socket_t  udp_socket;
static wiced_timed_event_t imu_read_event;
static wiced_timed_event_t battery_read_event;

static packet_t packet_id_imu_quaternion = IMU_QUATERNION;
static packet_t packet_id_limit_switch = LIMIT_SWITCH;
static packet_t packet_id_battery = BATTERY;

/******************************************************
 *               Function Definitions
 ******************************************************/

void application_start(void)
{
    /* Initialise the device and WICED framework */
    wiced_init( );

    //LCD_Init();                 // Inicjalizacja wyswietlacza
    //LCD_Clear(BG_Color);        // Czyszczenie ekranu
    //LCD_SetBackColor(BG_Color); // Ustawienie koloru tla
    //LCD_SetTextColor(Green);    // Ustawienie koloru tekstu
    //LCD_SetFont(&Font8x8);      // Ustawienie czcionki 8x8 pikseli

    //camera_init();

    /* Bring up the softAP and network interface */
    wiced_network_up( WICED_AP_INTERFACE, WICED_USE_INTERNAL_DHCP_SERVER, &device_init_ip_settings );

    wiced_pwm_init(WICED_PWM_1, 50, 9.0f);
    wiced_pwm_start(WICED_PWM_1);

    wiced_gpio_init(WICED_GPIO_25, INPUT_HIGH_IMPEDANCE);
    wiced_gpio_input_irq_enable(WICED_GPIO_25, IRQ_TRIGGER_BOTH_EDGES, limit_switch_handler, NULL);

    wiced_adc_init(WICED_ADC_1, 3);
    wiced_adc_init(WICED_ADC_2, 3);
    wiced_adc_init(WICED_ADC_3, 3);

    imu_on();


    /* Create UDP socket */
    if (wiced_udp_create_socket(&udp_socket, UDP_TARGET_PORT, WICED_AP_INTERFACE) != WICED_SUCCESS)
    {
        WPRINT_APP_INFO(("UDP socket creation failed\n"));
    }

    /* Register a function to send UDP packets */
    wiced_rtos_register_timed_event( &imu_read_event, WICED_NETWORKING_WORKER_THREAD, &tx_udp_timed_packet, IMU_READ_INTERVAL*MILLISECONDS, &packet_id_imu_quaternion );
    wiced_rtos_register_timed_event( &battery_read_event, WICED_NETWORKING_WORKER_THREAD, &tx_udp_timed_packet, BATTERY_READ_INTERVAL*MILLISECONDS, &packet_id_battery );

#ifdef GET_UDP_RESPONSE
    while (1)
    {
        /* Try to receive a UDP response */
        rx_udp_packet(NEVER_TIMEOUT);
    }
#endif
}


/*
 * Sends a UDP packet
 */
wiced_result_t tx_udp_timed_packet(void* arg)
{
    wiced_packet_t*          packet;
    char*                    udp_data;
    uint16_t                 available_data_length;
    const wiced_ip_address_t INITIALISER_IPV4_ADDRESS( target_ip_addr, UDP_TARGET_IP );
    packet_t packet_id = *((packet_t *)(arg));

    switch(packet_id)
    {
		case IMU_QUATERNION:
		{
			imu_data_t imu_data;

		    /* Create the UDP packet */
		    if (wiced_packet_create_udp(&udp_socket, UDP_MAX_DATA_LENGTH, &packet, (uint8_t**)&udp_data, &available_data_length) != WICED_SUCCESS)
		    {
		        WPRINT_APP_INFO(("UDP tx packet creation failed\n"));
		        return WICED_ERROR;
		    }

		    udp_data[0] = packet_id;

			imu_read(&imu_data);
			memcpy(udp_data + 1, imu_data.quat, 16);

		    /* Set the end of the data portion */
		    wiced_packet_set_data_end(packet, (uint8_t*)udp_data + UDP_MAX_DATA_LENGTH);

		    /* Send the UDP packet */
		    if (wiced_udp_send(&udp_socket, &target_ip_addr, UDP_TARGET_PORT, packet) != WICED_SUCCESS)
		    {
		        WPRINT_APP_INFO(("UDP packet send failed\n"));
		        wiced_packet_delete(packet);  /* Delete packet, since the send failed */
		        return WICED_ERROR;
		    }
			break;
		}
		case LIMIT_SWITCH:
		{
			uint8_t state = (uint8_t)wiced_gpio_input_get(WICED_GPIO_25);
			WPRINT_APP_INFO(("limit switch state: %d\r\n\n", state));

		    /* Create the UDP packet */
		    if (wiced_packet_create_udp(&udp_socket, UDP_MAX_DATA_LENGTH, &packet, (uint8_t**)&udp_data, &available_data_length) != WICED_SUCCESS)
		    {
		        WPRINT_APP_INFO(("UDP tx packet creation failed\n"));
		        return WICED_ERROR;
		    }

		    udp_data[0] = packet_id;

			memcpy(udp_data + 1, &state, 1);

		    /* Set the end of the data portion */
		    wiced_packet_set_data_end(packet, (uint8_t*)udp_data + UDP_MAX_DATA_LENGTH);

		    /* Send the UDP packet */
		    if (wiced_udp_send(&udp_socket, &target_ip_addr, UDP_TARGET_PORT, packet) != WICED_SUCCESS)
		    {
		        WPRINT_APP_INFO(("UDP packet send failed\n"));
		        wiced_packet_delete(packet);  /* Delete packet, since the send failed */
		        return WICED_ERROR;
		    }
			break;
		}
		case BATTERY:
		{
			uint16_t adc_value;
			float cell_voltage[3];

		    /* Create the UDP packet */
		    if (wiced_packet_create_udp(&udp_socket, UDP_MAX_DATA_LENGTH, &packet, (uint8_t**)&udp_data, &available_data_length) != WICED_SUCCESS)
		    {
		        WPRINT_APP_INFO(("UDP tx packet creation failed\n"));
		        return WICED_ERROR;
		    }

		    udp_data[0] = packet_id;

			wiced_adc_take_sample(WICED_ADC_1, &adc_value);
			cell_voltage[0] = (float)(adc_value) * 3.0f / (float)0xFFF;
			wiced_adc_take_sample(WICED_ADC_2, &adc_value);
			cell_voltage[1] = (float)(adc_value) * 3.0f / (float)0xFFF;
			wiced_adc_take_sample(WICED_ADC_3, &adc_value);
			cell_voltage[2] = (float)(adc_value) * 3.0f / (float)0xFFF;
			memcpy(udp_data + 1, cell_voltage, 12);
			//WPRINT_APP_INFO(("cell 1: %f\r\ncell 2: %f\r\ncell 3: %f\r\n\n", cell_voltage[0], cell_voltage[1], cell_voltage[2]));
		    /* Set the end of the data portion */
		    wiced_packet_set_data_end(packet, (uint8_t*)udp_data + UDP_MAX_DATA_LENGTH);

		    /* Send the UDP packet */
		    if (wiced_udp_send(&udp_socket, &target_ip_addr, UDP_TARGET_PORT, packet) != WICED_SUCCESS)
		    {
		        WPRINT_APP_INFO(("UDP packet send failed\n"));
		        wiced_packet_delete(packet);  /* Delete packet, since the send failed */
		        return WICED_ERROR;
		    }
			break;
		}
    }
    return WICED_SUCCESS;
}


/*
 * Attempts to receive a UDP packet
 */
wiced_result_t rx_udp_packet(uint32_t timeout)
{
    wiced_packet_t* packet;
    char* udp_data;
    uint16_t data_length;
    uint16_t available_data_length;

    /* Wait for UDP packet */
    wiced_result_t result = wiced_udp_receive(&udp_socket, &packet, timeout );
    if ((result == WICED_ERROR) || (result == WICED_TIMEOUT))
    {
    	WPRINT_APP_INFO(("Time out waiting to receive ...\n\n"));
        return result;
    }
    else
    {
    	packet_t packet_id;
        wiced_packet_get_data(packet, 0, (uint8_t**)&udp_data, &data_length, &available_data_length);
        memcpy(&packet_id, udp_data, 1);
        switch(packet_id)
        {
			case SERVO_ANGLE:
			{
				uint32_t servo_angle;
				memcpy(&servo_angle, udp_data + 1, 4);
				WPRINT_APP_INFO(("servo angle: %d\r\n", servo_angle));
				wiced_pwm_update_duty_cycle(WICED_PWM_1, (((float) servo_angle * 7.0f) / 180.0f) + 5.5f );
				break;
			}
		}
        wiced_packet_delete(packet);   /* Delete packet, it is no longer needed */
    }
    return WICED_SUCCESS;
}

void limit_switch_handler(void *arg)
{
	wiced_rtos_send_asynchronous_event(WICED_NETWORKING_WORKER_THREAD, &tx_udp_timed_packet, &packet_id_limit_switch);
}
