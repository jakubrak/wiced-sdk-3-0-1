#include "camera.h"
#include "MT9D111.h"
#include "wiced.h"
#include "task.h"

//extern uint8_t frame_buffer;

#define CAMERA_THREAD_PRIORITY (WICED_APPLICATION_PRIORITY)
#define CAMERA_STACK_SIZE (5000) /* printf requires 4K of stack */

uint32_t frame_size = 0;
uint32_t frame_number = 0;

void camera_frame_received(void *arg);
void camera_thread_main(uint32_t arg);

int camera_i2c_read_reg(unsigned char reg_addr, unsigned short *reg_val)
{
    wiced_i2c_message_t message;
    unsigned char data[2];
    wiced_i2c_init_rx_message(&message, data, 2, 0 , WICED_FALSE);
    message.slave_address = (uint16_t)MT9D111_DEVICE_ADDRESS << 1;
    message.register_address = (uint16_t)reg_addr;
    if(wiced_i2c_transfer(&wiced_i2c_camera, &message, 1) != WICED_SUCCESS)
    {
        WPRINT_APP_DEBUG(("I2C semaphore timeout\r\n"));
        return WICED_ERROR;
    }
    *reg_val = data[0] << 8;
    *reg_val += data[1];
    return WICED_SUCCESS;
}

int camera_i2c_write_reg(unsigned char reg_addr, unsigned short reg_val)
{
    wiced_i2c_message_t message;
    unsigned char data[2];
    data[0] = (reg_val >> 8) & 0xFF;
    data[1] = reg_val & 0xFF;
    wiced_i2c_init_tx_message(&message, data, 2, 0 , WICED_FALSE);
    message.slave_address = (uint16_t)MT9D111_DEVICE_ADDRESS << 1;
    message.register_address = (uint16_t)reg_addr;
    if(wiced_i2c_transfer(&wiced_i2c_camera, &message, 1) != WICED_SUCCESS)
    {
        WPRINT_APP_DEBUG(("I2C semaphore timeout\r\n"));
        return WICED_ERROR;
    }
    return WICED_SUCCESS;
}



void camera_delay_ms(unsigned long ms)
{
    wiced_rtos_delay_milliseconds(ms);
}

wiced_result_t camera_init()
{
	unsigned short reg_val;

	wiced_i2c_init(&wiced_i2c_camera);

	camera_i2c_write_reg(PAGE_SELECT_REG, 0x1);                // Select Page 1 - IFP
	camera_i2c_read_reg(UC_BOOT_MODE_REG, &reg_val);
	camera_i2c_write_reg(UC_BOOT_MODE_REG, (reg_val | 0x01));  // Reset microcontroller
	camera_delay_ms(100);

	wiced_dcmi_init(camera_frame_received);
	MT9D111_Init();
	WPRINT_APP_INFO(("Camera initialized\r\n"));
}

void camera_take_snapshot()
{
	WPRINT_APP_INFO(("Capture snapshot ... "));
	wiced_dcmi_take_snapshot();
	WPRINT_APP_INFO(("done.\r\n"));
}

void camera_preview()
{
	WPRINT_APP_INFO(("Switched to camera preview mode\r\n"));
	wiced_dcmi_preview();

}

void camera_frame_received(void *arg)
{
	frame_size = *((uint32_t *)arg);
}
